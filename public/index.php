<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-25 22:16
 */

error_reporting(-1);

define("APPLICATION_MICROTIME_START", microtime(true));
date_default_timezone_set('Europe/Warsaw');

define('APPLICATION_ENV_TEST',  'testing');
define('APPLICATION_ENV_DEV',   'development');
define('APPLICATION_ENV_PROD',  'production');


defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', __DIR__ . '/../application');

defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : APPLICATION_ENV_DEV));

if(APPLICATION_ENV == 'development'){
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
    ini_set('display_errors', 1);
}

chdir(APPLICATION_PATH);
set_include_path(implode(PATH_SEPARATOR, array(__DIR__ .'/../library')));
require_once('Zend/Loader/Autoloader.php');
$loader = Zend_Loader_Autoloader::getInstance();
$loader->setFallbackAutoloader(true);
$loader->suppressNotFoundWarnings(false);

try{
    $application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
    $application->bootstrap()->run();
}catch (Exception $e) {

    @header("HTTP/1.0 500 Oops !");
    @header('Content-type: text/html; charset=utf8');

    if(APPLICATION_ENV == 'production'){
        echo 'Wystąpił błąd. Pracujemy nad jego rozwiązaniem.';
    }else{
        echo '<style type="text/css">';
        echo 'body, pre {font-family:"Segoe UI";font-size:16px;}';
        echo 'h1 {font-family: "Segoe UI Light"}';
        echo 'pre {background:#EFEFEF;padding:10px;border:1px solid #CCC;}';
        echo '</style>';
        echo '<h1>Błąd aplikacji: '.get_class($e).'</h1>';
        echo '<pre>Message: '.$e->getMessage().'</pre>';
        echo '<pre>'.$e->getTraceAsString().'</pre>';
        echo '<pre>'.$e->getFile(). ' linia ' .$e->getLine().'</pre>';
        echo '<hr>';

        if($e = $e->getPrevious()){
            echo '<h2>Błąd aplikacji: '.get_class($e).'</h2>';
            echo '<pre>Message: '.$e->getMessage().'</pre>';
            echo '<pre>'.$e->getTraceAsString().'</pre>';
            echo '<pre>'.$e->getFile(). ' linia ' .$e->getLine().'</pre>';
        }
    }
}