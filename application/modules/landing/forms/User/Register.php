<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-28 11:37
 */

class Landing_Form_User_Register extends CMS_Form_Horizontal
{
    protected $_fast = false;
    public function __construct($fast = false){
        $this->_fast = $fast;
        parent::__construct();
    }
    public function init(){
        $this->setMethod(self::METHOD_POST);

        $this->addElement('email', 'email', array(
            'label' => 'E-mail',
            'placeholder' => 'adres e-mail',
            'required' => true
        ));

        $this->addElement('password', 'password', array(
            'label' => 'Hasło',
            'placeholder' => 'hasło',
            'required' => true,
            'maxlength' => 16,
            'validators' => array(
                array(
                    'validator' => 'password',
                    'options' => array(
                        'checkLength' => true,
                        'minLength' => 6,
                        'maxLength' => 16,
                    )
                )
            ),
        ));

        $this->addElement('password', 'repeat_password', array(
            'label' => 'Powtórz hasło',
            'required' => true,
            'placeholder' => 'powtórz hasło',
            'validators' => array(
                array(
                    'validator' => 'password',
                    'options' => array(
                        'contextCompare' => true,
                        'inputName' => 'password',
                        'checkLength' => false,
                    )
                ),
            )
        ));
        if(!$this->_fast){
            $this->addElement('text', 'first_name', array(
                'label' => 'Imię',
                'required' => false,
                'placeholder' => 'imię'
            ));
            $this->addElement('text', 'last_name', array(
                'label' => 'Nazwisko',
                'required' => false,
                'placeholder' => 'nazwisko'
            ));
            $this->addElement('text', 'phone', array(
                'label' => 'Telefon',
                'required' => false,
                'placeholder' => 'telefon'
            ));
        }

        $this->addSubmitButton('Zarejestruj');
    }

    public function isValid($data)
    {
        // checks if old_password is correct
        if($this->getModel()->users()->findByEmail($data['email'])){
            $this->getElement('email')->addError('User already exists.');
            return false;
        }

        return parent::isValid($data);
    }
}