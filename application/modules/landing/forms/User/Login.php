<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 13:12
 *
 */

class Landing_Form_User_Login extends CMS_Form_Horizontal
{
    public function init(){
        $this->setAction($this->getView()->route('user.login'));
        $this->setMethod('POST');

        $this->addElement('email', 'name', array(
            'label' => 'E-mail',
            'placeholder' => 'adres e-mail',
            'required' => true
        ));

        $this->addElement('password', 'password', array(
            'label' => 'Hasło',
            'placeholder' => 'hasło',
            'required' => true,
            'maxlength' => 16,
            'validators' => array(
                array(
                    'validator' => 'password',
                    'options' => array(
                        'checkLength' => true,
                        'minLength' => 6,
                        'maxLength' => 16,
                    )
                )
            ),
        ));
        $button = $this->createElement('button', 'formSubmit', array(
            'type' => 'submit',
            'label' => 'Zaloguj',
            'ignore' => true,
            'class' => 'btn'
        ));
        $this->addElement($button);
    }
    public function getUsername(){
        return $this->getElement('name');
    }
    public function getPassword(){
        return $this->getElement('password');
    }
//    public function addError($message){
//        $this->markAsError();
//        $this->getPassword()->markAsError();
//        $this->getUsername()->markAsError();
//        return parent::addError($message);
//    }
}