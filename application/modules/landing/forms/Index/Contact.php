<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-30 18:17
 */

class Landing_Form_Index_Contact extends CMS_Form_Horizontal
{
    public function init(){
        $this->setMethod(self::METHOD_POST);

        $this->addElement('email', 'email', array(
            'label' => 'Adres e-mail',
            'required' => true,
            'placeholder' => 'twój adres e-mail'
        ));
        $this->addElement('text', 'subject', array(
            'label' => 'Temat',
            'required' => true,
            'placeholder' => 'temat wiadomości'
        ));
        $this->addElement('textarea', 'message', array(
            'label' => 'Wiadomość',
            'required' => true,
            'placeholder' => 'treść wiadomości'
        ));
        $this->addSubmitButton();
    }
}