<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 13:07
 */

class AuthController extends CMS_Controller_Action
{
    public function init(){}

    public function loginAction(){
        $form = new Landing_Form_User_Login();
        $formRegister = new Landing_Form_User_Register(true);
        if($this->isValidPost($form)) {
            $data = (object)$form->getValues();
            $user = $this->getModel()->users()->findByEmail($data->name);
            /* @var $user Model_User */
            if($user) {
                if($user->getEnabled()) {
                    if ($this->authenticate($user, $data->password)) {
                        setcookie('authenticated', 1);
                        $user->setDateLastLogin(CMS_Date::now())->save();
                        $this->gotoRoute('panel.summary');
                    } else {
                        $user->setDateLastLoginFail(CMS_Date::now())->save();
                        $form->getPassword()->markAsError()->addError('Złe hasło!');
                    }
                }
                else{
                    $this->flashMessage('Konto zostało <b>wyłączone</b>! Nie można sie na nie zalogować!<br>Jeżeli nie wiesz dlaczego twoje konto zostało wyłączone skontaktuj się z administratorem.');
                    $this->gotoRouteAndExit('user.login');
                }
            }
            else{
                $form->getUsername()->markAsError()->addError('Taki użytkownik nie istnieje!');
            }
        }
        if($this->isValidPost($formRegister)){
            $hPassword = $this->getAuth()->saltPassword($formRegister->getValue('password'));
            $user = $this->getModel()->users()->createRow($formRegister->getValues());
            /* @var $user Model_User */
            $user->setPassword($hPassword)
                ->setRole(Model_User::ROLE_USER)
                ->setDateRegistered(CMS_Date::now())
                ->setDateLastLogin(CMS_Date::now())
                ->save();
            $data = $user->createData($formRegister->getValues());
            $data->save();

            $this->getAuth()->authenticate($user->getEmail(), $formRegister->getValue('password'));

            $this->flashMessage('Twoje konto zostało utworzone!');
            $this->gotoRouteAndExit('panel.summary');
        }
        $this->view->title = "Logowanie";
        $this->view->form = $form;
        $this->view->formRegister = $formRegister;
    }

    public function logoutAction(){
        if($this->getAuth()->hasIdentity()){
            $this->getAuth()->clearIdentity();
        }
        $this->flashMessage('Wylogowano pomyślnie!');
        $this->gotoRoute('user.login');
    }

    public function registerAction(){
        if(Zend_Registry::get('UserCanRegister')) {
            $form = new Landing_Form_User_Register();

            if ($this->isValidPost($form)) {

                $hPassword = $this->getAuth()->saltPassword($form->getValue('password'));
                $user = $this->getModel()->users()->createRow($form->getValues());
                /* @var $user Model_User */
                $user->setPassword($hPassword)
                     ->setRole(Model_User::ROLE_USER)
                     ->setDateRegistered(CMS_Date::now())
                     ->setDateLastLogin(CMS_Date::now())
                     ->save();
                $data = $user->createData($form->getValues());
                $data->save();

                $this->getAuth()->authenticate($user->getEmail(), $form->getValue('password'));

                $this->flashMessage('Twoje konto zostało utworzone!');
                $this->gotoRouteAndExit('panel.summary');
            }

            $this->view->form = $form;
        }
    }
}