<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-25 22:23
 *
 * @method Service_ModelRepository getModel()
 */

class IndexController extends CMS_Controller_Action
{
    public function init(){}

    public function indexAction(){

        $site = $this->getModel()->sites()->findBySlug('home');
        if(!$site) {
            $site = $this->getModel()->sites()->createRow();
            $site
                ->setSlug('home')
                ->setTitle('Strona Główna')
                ->setColumnOne('    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed fermentum augue. Duis a mattis metus, sed condimentum erat. Fusce viverra nisl non rhoncus pharetra. Aliquam tincidunt risus et vehicula varius. Nam sit amet nisl sapien. Quisque mollis dictum massa, et blandit urna consectetur vitae. Ut augue tortor, suscipit nec est id, posuere rhoncus sem. Cras tempor justo id purus commodo, quis condimentum risus fermentum. Pellentesque ac nunc sed massa consequat mattis. Phasellus ultricies lectus et eros dapibus vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec eget enim eget purus gravida interdum et a lorem. Phasellus efficitur elit et lectus pretium, id viverra arcu tempus. Cras tristique leo arcu, et convallis dui cursus vitae. Suspendisse id urna ultrices, commodo tellus nec, mattis nisi. Proin vitae mi tempor, tempor tellus nec, porta est.</p>
    <p>Phasellus sagittis velit sed erat pharetra, quis feugiat purus porttitor. Nulla dapibus, est nec vehicula condimentum, nunc ligula mollis arcu, eget auctor tortor mi et leo. Sed sit amet nisi in arcu volutpat vestibulum. Phasellus et libero lobortis, cursus nisi id, malesuada nunc. In porta quis erat tincidunt pharetra. Suspendisse posuere blandit velit nec iaculis. Donec tempor leo at convallis placerat. Donec at maximus velit, id pellentesque turpis. Phasellus fringilla, neque gravida facilisis eleifend, magna leo feugiat urna, ac ultricies nibh sem varius risus. Aenean vel dictum ipsum. Pellentesque eu augue massa. Proin euismod scelerisque pellentesque. Suspendisse massa est, tempus non quam pulvinar, rhoncus ullamcorper quam.</p>')
                ->setColumnTwo('<p>Etiam malesuada vel libero nec facilisis. Sed in lorem dolor. In pulvinar id arcu consectetur mattis. Pellentesque eu tellus est. Fusce placerat tortor in arcu interdum, vitae ullamcorper lectus ullamcorper. Aliquam viverra sollicitudin mauris nec dapibus. Suspendisse ac maximus neque. Vivamus vulputate metus vel ex ultrices, nec malesuada enim dictum. Etiam accumsan enim quis tincidunt laoreet. Pellentesque eu facilisis dui, sit amet condimentum justo. Aenean ut porttitor velit, nec interdum neque. Pellentesque malesuada ante nec feugiat egestas. Pellentesque vel est at nibh fermentum pretium. Integer urna enim, maximus id accumsan a, maximus quis augue.</p>
    <p>Phasellus elementum suscipit vehicula. Vestibulum vestibulum sagittis nisi sit amet egestas. Morbi purus leo, rutrum ac imperdiet in, eleifend a nisi. Donec dapibus eu nisi id maximus. Pellentesque pharetra elit at turpis commodo viverra at non nisi. Vivamus cursus, massa id sagittis tempus, sapien justo fermentum nulla, consectetur vehicula neque tortor a nisi. Nulla tempus leo nulla, et pharetra dui convallis et. Integer turpis odio, porttitor egestas ultrices id, elementum tempus augue.</p>
    <p>Integer scelerisque, felis at scelerisque imperdiet, risus urna tincidunt turpis, at tincidunt libero lorem vitae ex. Mauris vulputate egestas nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec cursus varius hendrerit. Fusce nec augue et est fermentum commodo a a sapien. Sed ut tortor at dolor feugiat gravida. Maecenas consectetur lorem id aliquam fermentum.</p>')
                ->setDateCreated(CMS_Date::now())
                ->save();
        }
        $this->view->site = $site;
    }
    public function aboutAction(){
        $site = $this->getModel()->sites()->findBySlug('about');
        if(!$site) {
            $site = $this->getModel()->sites()->createRow();
            $site
                ->setSlug('about')
                ->setTitle('O Nas')
                ->setColumnOne('    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed fermentum augue. Duis a mattis metus, sed condimentum erat. Fusce viverra nisl non rhoncus pharetra. Aliquam tincidunt risus et vehicula varius. Nam sit amet nisl sapien. Quisque mollis dictum massa, et blandit urna consectetur vitae. Ut augue tortor, suscipit nec est id, posuere rhoncus sem. Cras tempor justo id purus commodo, quis condimentum risus fermentum. Pellentesque ac nunc sed massa consequat mattis. Phasellus ultricies lectus et eros dapibus vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec eget enim eget purus gravida interdum et a lorem. Phasellus efficitur elit et lectus pretium, id viverra arcu tempus. Cras tristique leo arcu, et convallis dui cursus vitae. Suspendisse id urna ultrices, commodo tellus nec, mattis nisi. Proin vitae mi tempor, tempor tellus nec, porta est.</p>
    <p>Phasellus sagittis velit sed erat pharetra, quis feugiat purus porttitor. Nulla dapibus, est nec vehicula condimentum, nunc ligula mollis arcu, eget auctor tortor mi et leo. Sed sit amet nisi in arcu volutpat vestibulum. Phasellus et libero lobortis, cursus nisi id, malesuada nunc. In porta quis erat tincidunt pharetra. Suspendisse posuere blandit velit nec iaculis. Donec tempor leo at convallis placerat. Donec at maximus velit, id pellentesque turpis. Phasellus fringilla, neque gravida facilisis eleifend, magna leo feugiat urna, ac ultricies nibh sem varius risus. Aenean vel dictum ipsum. Pellentesque eu augue massa. Proin euismod scelerisque pellentesque. Suspendisse massa est, tempus non quam pulvinar, rhoncus ullamcorper quam.</p>
    <p>Etiam malesuada vel libero nec facilisis. Sed in lorem dolor. In pulvinar id arcu consectetur mattis. Pellentesque eu tellus est. Fusce placerat tortor in arcu interdum, vitae ullamcorper lectus ullamcorper. Aliquam viverra sollicitudin mauris nec dapibus. Suspendisse ac maximus neque. Vivamus vulputate metus vel ex ultrices, nec malesuada enim dictum. Etiam accumsan enim quis tincidunt laoreet. Pellentesque eu facilisis dui, sit amet condimentum justo. Aenean ut porttitor velit, nec interdum neque. Pellentesque malesuada ante nec feugiat egestas. Pellentesque vel est at nibh fermentum pretium. Integer urna enim, maximus id accumsan a, maximus quis augue.</p>
    <p>Phasellus elementum suscipit vehicula. Vestibulum vestibulum sagittis nisi sit amet egestas. Morbi purus leo, rutrum ac imperdiet in, eleifend a nisi. Donec dapibus eu nisi id maximus. Pellentesque pharetra elit at turpis commodo viverra at non nisi. Vivamus cursus, massa id sagittis tempus, sapien justo fermentum nulla, consectetur vehicula neque tortor a nisi. Nulla tempus leo nulla, et pharetra dui convallis et. Integer turpis odio, porttitor egestas ultrices id, elementum tempus augue.</p>
    <p>Integer scelerisque, felis at scelerisque imperdiet, risus urna tincidunt turpis, at tincidunt libero lorem vitae ex. Mauris vulputate egestas nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec cursus varius hendrerit. Fusce nec augue et est fermentum commodo a a sapien. Sed ut tortor at dolor feugiat gravida. Maecenas consectetur lorem id aliquam fermentum.</p>')
                ->setDateCreated(CMS_Date::now())
                ->save();
        }
        $this->view->site = $site;
    }
    public function contactAction(){
        $form = new Landing_Form_Index_Contact();
        if($this->isValidPost($form)){
            $data = $form->getValues();
            $send = $this->getModel()->getMailer()->sendContactEmail($data, 'michau.swiatek@gmail.com', $data['subject']);
            if($send){
                $this->flashMessage('Wiadomość została wysłana!');
            }
            else $this->flashMessage('Nie można było wysłać wiadomości');
            $this->gotoRouteAndExit();
        }
        $site = $this->getModel()->sites()->findBySlug('contact');
        if(!$site) {
            $site = $this->getModel()->sites()->createRow();
            $site
                ->setSlug('contact')
                ->setTitle('Kontakt')
                ->setColumnOne('<p>Etiam malesuada vel libero nec facilisis. Sed in lorem dolor. In pulvinar id arcu consectetur mattis. Pellentesque eu tellus est. Fusce placerat tortor in arcu interdum, vitae ullamcorper lectus ullamcorper. Aliquam viverra sollicitudin mauris nec dapibus. Suspendisse ac maximus neque. Vivamus vulputate metus vel ex ultrices, nec malesuada enim dictum. Etiam accumsan enim quis tincidunt laoreet. Pellentesque eu facilisis dui, sit amet condimentum justo. Aenean ut porttitor velit, nec interdum neque. Pellentesque malesuada ante nec feugiat egestas. Pellentesque vel est at nibh fermentum pretium. Integer urna enim, maximus id accumsan a, maximus quis augue.</p>')
                ->setDateCreated(CMS_Date::now())
                ->save();
        }
        $this->view->site = $site;
        $this->view->form = $form;
    }

    public function siteAction(){
        $slug = $this->getParam('slug');
        if($slug == 'home') $this->gotoRouteAndExit('home');
        if($slug == 'about') $this->gotoRouteAndExit('about');
        if($slug == 'contact') $this->gotoRouteAndExit('contact');
        $site = $this->getModel()->sites()->findBySlug($slug);
        if($site){
            $this->view->site = $site;
        }
        else{
            $this->flashMessage('Strona której szukasz nie istnieje.');
            $this->gotoRouteAndExit('home');
        }
    }
}