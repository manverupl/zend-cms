<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-25 22:37
 */

class ErrorController extends CMS_Controller_Error
{
    public function init(){}

    public function errorAction(){
        $this->view->title = "Error";
        if($this->canDisplayException()){
            $this->view->exception = $this->getException();
            $this->view->message = $this->getException()->getMessage();
        }
        else{
            $this->view->exception = null;
            $this->view->message = $this->getException()->getMessage();
        }
    }
}