<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-31 20:56
 * @method Service_ModelRepository getModel()
 */

class NewsController extends CMS_Controller_Action
{
    public function init(){}

    public function indexAction(){
        if(!$this->getParam('page')) $this->setParam('page', 1);
        $news = $this->getModel()->news()->findAll(null, 'date_created ASC');
        $paginator = $this->getPaginator($news, null, 5);
        $this->view->news = $paginator;
    }

    public function newsAction(){
        $id = $this->getParam('id');
        $news = $this->getModel()->news()->findById($id);
        if($news){
            $this->view->news = $news;
        }
        else{
            $this->flashMessage('Aktualność której szukasz nie istnieje!');
            $this->gotoRouteAndExit('news.list', array('page' => 1));
        }
    }
}