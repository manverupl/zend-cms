<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-31 12:37
 */

class Admin_Form_Landing_About extends Admin_Form_Landing_Site
{

    public function init(){
        parent::init();
        $this->getElement('slug')->setAttrib('disabled', true);
        $this->removeElement('column_two');
        $this->removeElement('column_one');
        $this->addElement('textarea', 'column_one', array(
            'label' => 'Treść',
            'placeholder' => 'wyświetlana treść',
            'required' => true,
            'value' => $this->site->getColumnOne(),
        ));
        $this->removeSubmitButton();
        $this->addSubmitButton('Zapisz');
    }

    public function isValid($data){
        if(!isset($data['slug']) || $data['slug'] != 'about') $data['slug'] = 'about';
        return parent::isValid($data);
    }
}