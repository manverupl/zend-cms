<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-31 12:35
 *
 * @method Service_ModelRepository getModel
 */

class Admin_Form_Landing_Add extends CMS_Form_Horizontal
{

    public function init()
    {
        $this->setMethod(self::METHOD_POST);

        $this->addElement('text', 'title', array(
            'label' => 'Tytuł strony',
            'required' => true,
            'placeholder' => 'tytuł strony',
        ));
        $this->addElement('text', 'slug', array(
            'label' => 'Slug',
            'required' => true,
            'placeholder' => 'slug',
        ));
        $this->addElement('textarea', 'column_one', array(
            'label' => 'Kolumna lewa',
            'required' => true,
            'placeholder' => 'tekst w lewej kolumnie',
        ));
        $this->addElement('textarea', 'column_two', array(
            'label' => 'Kolumna prawa',
            'required' => false,
            'placeholder' => 'tekst w prawej kolumnie',
        ));

        $this->addSubmitButton('Dodaj');
    }

    public function isValid($data)
    {
        if ($this->getModel()->sites()->findBySlug($data['slug'])) {
            $this->getElement('slug')->markAsError()->addError('Strona o takim slug istnieje już w bazie danych!');
            return false;
        }
        return parent::isValid($data);
    }
}