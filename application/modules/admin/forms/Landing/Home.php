<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-31 12:37
 */

class Admin_Form_Landing_Home extends Admin_Form_Landing_Site
{

    public function init(){
        parent::init();
        $this->getElement('slug')->setAttrib('disabled', true);
        $this->getElement('column_two')->setAttrib('required', true);
    }

    public function isValid($data){
        if(!isset($data['slug']) || $data['slug'] != 'home') $data['slug'] = 'home';
        return parent::isValid($data);
    }
}