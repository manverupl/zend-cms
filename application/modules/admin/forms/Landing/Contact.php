<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-31 12:37
 */

class Admin_Form_Landing_Contact extends Admin_Form_Landing_Site
{

    public function init(){
        parent::init();
        $this->getElement('slug')->setAttrib('disabled', true);
        $this->removeElement('column_one');
        $this->removeElement('column_two');
        $this->addElement('textarea', 'column_one', array(
            'label' => 'Kolumna prawa',
            'placeholder' => 'treść prawej kolumny',
            'required' => true,
            'value' => $this->site->getColumnOne(),
        ));
        $this->removeSubmitButton();
        $this->addSubmitButton('Zapisz');
    }

    public function isValid($data){
        if(!isset($data['slug']) || $data['slug'] != 'contact') $data['slug'] = 'contact';
        return parent::isValid($data);
    }
}