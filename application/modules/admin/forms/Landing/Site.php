<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-31 12:22
 *
 * @method Service_ModelRepository getModel()
 */

class Admin_Form_Landing_Site extends CMS_Form_Horizontal
{
    /**
     * @var Model_Site
     */
    protected $site;
    public function __construct(Model_Site $site){
        $this->site = $site;
        parent::__construct();
    }

    public function init(){
        $this->setMethod(self::METHOD_POST);

        $this->addElement('text', 'title', array(
            'label' => 'Tytuł strony',
            'required' => true,
            'placeholder' => 'tytuł strony',
            'value' => $this->site->getTitle()
        ));
        $this->addElement('text', 'slug', array(
            'label' => 'Slug',
            'required' => true,
            'placeholder' => 'slug',
            'value' => $this->site->getSlug()
        ));
        $this->addElement('textarea', 'column_one', array(
            'label' => 'Kolumna lewa',
            'required' => true,
            'placeholder' => 'tekst w lewej kolumnie',
            'value' => $this->site->getColumnOne()
        ));
        $this->addElement('textarea', 'column_two', array(
            'label' => 'Kolumna prawa',
            'required' => false,
            'placeholder' => 'tekst w prawej kolumnie',
            'value' => $this->site->getColumnTwo()
        ));

        $this->addSubmitButton('Zapisz');
    }

    public function isValid($data){
        if($data['slug'] != $this->site->getSlug() && $this->getModel()->sites()->findBySlug($data['slug'])){
            $this->getElement('slug')->markAsError()->addError('Taki slug istnieje już w bazie danych!');
            return false;
        }
        return parent::isValid($data);
    }

}