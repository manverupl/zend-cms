<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-30 15:35
 */

class Admin_Form_User_Edit extends CMS_Form_Horizontal
{
    /**
     * @var Model_User
     */
    protected $user;
    public function __construct(Model_User $user){
        $this->user = $user;
        parent::__construct();
    }

    public function init(){
        $this->setMethod(self::METHOD_POST);

        $this->addElement('email', 'email', array(
            'label' => 'Adres e-mail',
            'placeholder' => 'adres e-mail',
            'required' => true,
            'value' => $this->user->getEmail()
        ));

        $roleArray = array(
            Model_User::ROLE_USER => 'Użytkownik',
            Model_User::ROLE_ADMIN => 'Administrator',
            Model_User::ROLE_SUPERADMIN => 'Super Administrator'
        );

        $this->addElement('select', 'role', array(
            'label' => 'Rola',
            'required' => true,
            'multioptions' => $roleArray,
            'value' => $this->user->getRole()
        ));

        $this->addElement('password', 'password', array(
            'label' => 'Hasło',
            'placeholder' => 'hasło',
            'required' => false,
            'maxlength' => 16,
            'validators' => array(
                array(
                    'validator' => 'password',
                    'options' => array(
                        'checkLength' => true,
                        'minLength' => 6,
                        'maxLength' => 16,
                    )
                )
            ),
        ));

        $this->addElement('checkbox', 'enabled', array(
            'label' => 'Konto włączone?',
            'checked' => $this->user->getEnabled()
        ));

        $this->addElement('checkbox', 'active', array(
            'label' => 'Konto aktywne?',
            'checked' => $this->user->getActive()
        ));

        $this->addElement('text', 'first_name', array(
            'label' => 'Imię',
            'required' => false,
            'placeholder' => 'imię',
            'value' => $this->user->findOneData()->getFirstName(),
        ));

        $this->addElement('text', 'last_name', array(
            'label' => 'Nazwisko',
            'required' => false,
            'placeholder' => 'nazwisko',
            'value' => $this->user->findOneData()->getLastName(),
        ));

        $this->addElement('text', 'phone', array(
            'label' => 'Telefon',
            'required' => false,
            'placeholder' => 'telefon',
            'value' => $this->user->findOneData()->getPhone(),
        ));

        $this->addSubmitButton('Zapisz');
    }

    public function isValid($data){

        if($data['email'] != $this->user->getEmail()){
            $user = $this->getModel()->users()->findByEmail($data['email']);
            if($user){
                $this->getElement('email')->addError('Taki email istnieje już w bazie danych!');
                return false;
            }
        }

        return parent::isValid($data);
    }
}