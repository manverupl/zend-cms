<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-30 15:35
 */

class Admin_Form_User_Add extends CMS_Form_Horizontal
{

    public function init(){
        $this->setMethod(self::METHOD_POST);

        $this->addElement('email', 'email', array(
            'label' => 'Adres e-mail',
            'placeholder' => 'adres e-mail',
            'required' => true,
        ));

        $roleArray = array(
            Model_User::ROLE_USER => 'Użytkownik',
            Model_User::ROLE_ADMIN => 'Administrator',
            Model_User::ROLE_SUPERADMIN => 'Super Administrator'
        );

        $this->addElement('select', 'role', array(
            'label' => 'Rola',
            'required' => true,
            'multioptions' => $roleArray,
            'value' => Model_User::ROLE_USER
        ));

        $this->addElement('password', 'password', array(
            'label' => 'Hasło',
            'placeholder' => 'hasło',
            'required' => true,
            'maxlength' => 16,
            'validators' => array(
                array(
                    'validator' => 'password',
                    'options' => array(
                        'checkLength' => true,
                        'minLength' => 6,
                        'maxLength' => 16,
                    )
                )
            ),
        ));

        $this->addElement('checkbox', 'enabled', array(
            'label' => 'Konto włączone?',
            'checked' => true
        ));

        $this->addElement('checkbox', 'active', array(
            'label' => 'Konto aktywne?',
            'checked' => false
        ));

        $this->addElement('text', 'first_name', array(
            'label' => 'Imię',
            'required' => false,
            'placeholder' => 'imię',
        ));

        $this->addElement('text', 'last_name', array(
            'label' => 'Nazwisko',
            'required' => false,
            'placeholder' => 'nazwisko',
        ));

        $this->addElement('text', 'phone', array(
            'label' => 'Telefon',
            'required' => false,
            'placeholder' => 'telefon',
        ));

        $this->addSubmitButton('Dodaj Użytkownika');
    }

    public function isValid($data){

        $user = $this->getModel()->users()->findByEmail($data['email']);
        if($user) {
            $this->getElement('email')->addError('Taki email istnieje już w bazie danych!');
            return false;
        }

        return parent::isValid($data);
    }
}