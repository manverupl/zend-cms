<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-04-01 13:48
 */

class Admin_Form_News_Add extends CMS_Form_Horizontal
{

    public function init(){

        $this->setMethod(self::METHOD_POST);

        $this->addElement('text', 'title', array(
            'label' => 'Tytuł',
            'required' => true,
            'placeholder' => 'tytuł aktualności'
        ));

        $this->addElement('text', 'author', array(
            'label' => 'Autor',
            'required' => true,
            'placeholder' => 'autor tekstu'
        ));
        $this->addElement('textarea', 'short_text', array(
            'label' => 'Zajawka',
            'required' => false,
            'placeholder' => 'Tekst wyświetlany na liscie aktualności jako jej skrót. Jeśli zostawisz to pole puste to w to miejsce trafi pierwsze 20 słów aktualności.'
        ));
        $this->addElement('textarea', 'text', array(
            'label' => 'Treść',
            'required' => true,
            'placeholder' => 'Tekst aktualności'
        ));

        $this->addSubmitButton('Dodaj');

    }

}