<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-04-01 13:48
 */

class Admin_Form_News_Edit extends CMS_Form_Horizontal
{
    /**
     * @var Model_News
     */
    protected $news;

    public function __construct(Model_News $news){
        $this->news = $news;

        parent::__construct();
    }

    public function init(){

        $this->setMethod(self::METHOD_POST);

        $this->addElement('text', 'title', array(
            'label' => 'Tytuł',
            'required' => true,
            'placeholder' => 'tytuł aktualności',
            'value' => $this->news->getTitle()
        ));

        $this->addElement('text', 'author', array(
            'label' => 'Autor',
            'required' => true,
            'placeholder' => 'autor tekstu',
            'value' => $this->news->getAuthor()
        ));
        $this->addElement('textarea', 'short_text', array(
            'label' => 'Zajawka',
            'required' => false,
            'placeholder' => 'Tekst wyświetlany na liscie aktualnosci jako jej skrót. Jeśli zostawisz to pole puste to w to miejsce trafi pierwsze 20 słów aktualnosci.',
            'value' => $this->news->getShortText()
        ));
        $this->addElement('textarea', 'text', array(
            'label' => 'Treść',
            'required' => true,
            'placeholder' => 'Tekst aktualnosći',
            'value' => $this->news->getText()
        ));

        $this->addSubmitButton('Zapisz');

    }

}