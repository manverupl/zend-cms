<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-04-01 13:23
 *
 * @method Service_ModelRepository getModel()
 */

class Admin_NewsController extends CMS_Controller_Action
{
    public function init(){
        if(!$this->getAuth()->hasIdentity()){
            $this->gotoRouteAndExit('user.login');
        }
        $us = $this->getModel()->user();
        if($us->getRole() == Model_User::ROLE_USER){
            $this->gotoRouteAndExit('panel.summary');
        }
    }
    public function indexAction(){
        $news = $this->getModel()->news()->findAll(null, 'date_created ASC');
        $paginator = $this->getPaginator($news, null, 10);
        $this->view->news = $paginator;
    }

    public function editAction(){
        $id = $this->getParam('id');
        $news = $this->getModel()->news()->findById($id);
        if($news){
            $form = new Admin_Form_News_Edit($news);
            if($this->isValidPost($form)){
                $data = $form->getValues();
                if(!isset($data['short_text']) || $data['short_text'] == ''){
                    $short_text = implode(' ', array_slice(explode(' ', $data['text']), 0, 20));
                    $data['short_text'] = $short_text;
                }
                $news->setFromArray($data);
                $news->setDateModified(CMS_Date::now())->save();
                $this->flashMessage('Zmiany zostały zapisane');
                $this->gotoRouteAndExit('admin.news.list');
            }
            $this->view->form = $form;
            $this->view->news = $news;
        }
        else{
            $this->flashMessage('Aktualność o zadanym id nie istnieje.');
            $this->gotoRouteAndExit('admin.news.list');
        }
    }
    public function addAction(){
        $form = new Admin_Form_News_Add();
        if($this->isValidPost($form)){
            $data = $form->getValues();
            if(!isset($data['short_text']) || $data['short_text'] == ''){
                $short_text = implode(' ', array_slice(explode(' ', $data['text']), 0, 20));
                $data['short_text'] = $short_text . " ...";
            }
            $news = $this->getModel()->news()->createRow($data);
            $news->setDateCreated(CMS_Date::now())->save();
            $this->flashMessage('Aktualność została dodana');
            $this->gotoRouteAndExit('admin.news.list');
        }
        $this->view->form = $form;
    }
}