<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-28 23:36
 */

class Admin_IndexController extends CMS_Controller_Action
{
    public function init(){
        if(!$this->getAuth()->hasIdentity()){
            $this->gotoRouteAndExit('user.login');
        }
        $us = $this->getModel()->user();
        if($us->getRole() == Model_User::ROLE_USER){
            $this->gotoRouteAndExit('panel.summary');
        }
    }

    public function indexAction(){

    }
}