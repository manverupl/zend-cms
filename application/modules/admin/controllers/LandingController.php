<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-28 23:38
 *
 * @method Service_ModelRepository getModel()
 */

class Admin_LandingController extends CMS_Controller_Action
{
    public function init(){
        if(!$this->getAuth()->hasIdentity()){
            $this->gotoRouteAndExit('user.login');
        }
        $us = $this->getModel()->user();
        if($us->getRole() == Model_User::ROLE_USER){
            $this->gotoRouteAndExit('panel.summary');
        }
    }

    public function indexAction(){
        $sites = $this->getModel()->sites()->findAll();
        $paginator = $this->getPaginator($sites, null, 10);
        $this->view->sites = $paginator;
    }

    public function homeAction(){
        $site = $this->getModel()->sites()->findBySlug('home');
        if($site){
            $form = new Admin_Form_Landing_Home($site);
            $form->setModel($this->getModel());
            if($this->isValidPost($form)){
                $data = $form->getValues();
                $site->setFromArray($data);
                $site->setDateModified(CMS_Date::now())->save();
                $this->flashMessage('Zmiany zostały zapisane!');
                $this->gotoRouteAndExit('admin.landing.list');
            }
            $this->view->site = $site;
            $this->view->form = $form;
        }
        else{
            $this->flashMessage('Strona główna nie istnieje w bazie danych. Nie można jej edytować.<br>Prawdopodobnie nie została jeszcze odwiedzona, aby pojawił się wpis w bazie danych strona ta musi zostać odwiedzona, bądz należy dodać wpis ręcznie.');
            $this->gotoRouteAndExit('admin.landing.list');
        }
    }

    public function aboutAction(){
        $site = $this->getModel()->sites()->findBySlug('about');
        if($site){
            $form = new Admin_Form_Landing_About($site);
            $form->setModel($this->getModel());
            if($this->isValidPost($form)){
                $data = $form->getValues();
                $site->setFromArray($data);
                $site->setDateModified(CMS_Date::now())->save();
                $this->flashMessage('Zmiany zostały zapisane!');
                $this->gotoRouteAndExit('admin.landing.list');
            }
            $this->view->site = $site;
            $this->view->form = $form;
        }
        else{
            $this->flashMessage('Strona "O Nas" nie istnieje w bazie danych. Nie można jej edytować.<br>Prawdopodobnie nie została jeszcze odwiedzona, aby pojawił się wpis w bazie danych strona ta musi zostać odwiedzona, bądz należy dodać wpis ręcznie.');
            $this->gotoRouteAndExit('admin.landing.list');
        }
    }

    public function contactAction(){
        $site = $this->getModel()->sites()->findBySlug('contact');
        if($site){
            $form = new Admin_Form_Landing_Contact($site);
            $form->setModel($this->getModel());
            if($this->isValidPost($form)){
                $data = $form->getValues();
                $site->setFromArray($data);
                $site->setDateModified(CMS_Date::now())->save();
                $this->flashMessage('Zmiany zostały zapisane!');
                $this->gotoRouteAndExit('admin.landing.list');
            }
            $this->view->site = $site;
            $this->view->form = $form;
        }
        else{
            $this->flashMessage('Strona "Kontakt" nie istnieje w bazie danych. Nie można jej edytować.<br>Prawdopodobnie nie została jeszcze odwiedzona, aby pojawił się wpis w bazie danych strona ta musi zostać odwiedzona, bądz należy dodać wpis ręcznie.');
            $this->gotoRouteAndExit('admin.landing.list');
        }
    }

    public function siteAction(){
        $slug = $this->getParam('slug');
        $site = $this->getModel()->sites()->findBySlug($slug);
        if($site){
            $form = new Admin_Form_Landing_Site($site);
            $form->setModel($this->getModel());
            if($this->isValidPost($form)){
                $data = $form->getValues();
                $site->setFromArray($data);
                $site->setDateModified(CMS_Date::now())->save();
                $this->flashMessage('Zmiany zostały zapisane!');
                $this->gotoRouteAndExit('admin.landing.list');
            }
            $this->view->site = $site;
            $this->view->form = $form;
        }
        else{
            $this->flashMessage('Strona o zadanym slug nie istnieje. Nie można jej edytować.');
            $this->gotoRouteAndExit('admin.landing.list');
        }
    }

    public function addAction(){
        $form = new Admin_Form_Landing_Add();
        $form->setModel($this->getModel());
        if($this->isValidPost($form)){
            $data = $form->getValues();
            $site = $this->getModel()->sites()->createRow($data);
            $site->setDateCreated(CMS_Date::now())->save();
            $this->flashMessage('Strona została utworzona');
            $this->gotoRouteAndExit('admin.landing.list');
        }
        $this->view->form = $form;
    }
}