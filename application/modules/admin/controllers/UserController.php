<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-28 23:38
 */

class Admin_UserController extends CMS_Controller_Action
{
    public function init(){
        if(!$this->getAuth()->hasIdentity()){
            $this->gotoRouteAndExit('user.login');
        }
        $us = $this->getModel()->user();
        if($us->getRole() == Model_User::ROLE_USER){
            $this->gotoRouteAndExit('panel.summary');
        }
    }

    public function indexAction(){
        $users = $this->getModel()->users()->findAll();
        $paginator = $this->getPaginator($users, null, 10);
        $this->view->users = $paginator;
    }

    public function addAction(){
        $form = new Admin_Form_User_Add();
        $form->setModel($this->getModel());
        if($this->isValidPost($form)){
            $data = $form->getValues();
            $hP = $this->getAuth()->saltPassword($data['password']);
            $user = $this->getModel()->users()->createRow($data);
            /* @var $user Model_User */
            $user->setPassword($hP)->setDateRegistered(CMS_Date::now())->save();

            $user->createData($form->getValues())->save();

            $this->flashMessage('Użytkownik został utworzony.');
            $this->gotoRouteAndExit('admin.users.list');
        }
        $this->view->form = $form;
    }

    public function editAction(){
        $id = $this->getParam('id');
        $user = $this->getModel()->users()->findById($id);
        /* @var $user Model_User */
        if($user) {
            if(!$user->findOneData()){
                $data = $user->createData();
                $data->save();
            }
            $form = new Admin_Form_User_Edit($user);
            $form->setModel($this->getModel());
            if($this->isValidPost($form)){
                $data = $form->getValues();
                if($data['password'] != ''){
                    $hoP = $this->getAuth()->saltPassword($data['password']);
                    $user->setPassword($hoP);
                }
                $user->setEmail($data['email'])->setRole($data['role'])->setEnabled($data['enabled'])->setActive($data['active'])
                    ->setDateModified(CMS_Date::now())->save();
                $user_data = $user->findOneData();
                $user_data->setFromArray($data)->save();

                $this->flashMessage('Zmiany zostały zapisane!');
                $this->gotoRouteAndExit('admin.users.details', array('id' => $user->getId()));
            }
            $this->view->user = $user;
            $this->view->form = $form;
        }
        else{
            $this->flashMessage('Użytkownik o zadanym id nie istnieje!');
            $this->gotoRouteAndExit('admin.users.list');
        }
    }

    public function detailsAction(){
        $id = $this->getParam('id');
        $user = $this->getModel()->users()->findById($id);
        if($user) {
            $this->view->user = $user;
        }
        else{
            $this->flashMessage('Użytkownik o zadanym id nie istnieje!');
            $this->gotoRouteAndExit('admin.users.list');
        }
    }
}