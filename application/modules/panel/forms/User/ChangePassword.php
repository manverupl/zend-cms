<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-29 17:06
 */

class Panel_Form_User_ChangePassword extends CMS_Form_Horizontal
{
    public function init(){
        $this->setMethod(self::METHOD_POST);

        $this->addElement('password', 'old_password', array(
            'label' => 'Stare hasło',
            'placeholder' => 'stare hasło',
            'required' => true,
            'maxlength' => 16,
        ));

        $this->addElement('password', 'password', array(
            'label' => 'Nowe hasło',
            'placeholder' => 'nowe hasło',
            'required' => true,
            'maxlength' => 16,
            'validators' => array(
                array(
                    'validator' => 'password',
                    'options' => array(
                        'checkLength' => true,
                        'minLength' => 6,
                        'maxLength' => 16,
                    )
                )
            ),
        ));

        $this->addElement('password', 'repeat_password', array(
            'label' => 'Powtórz hasło',
            'placeholder' => 'powtórz hasło',
            'required' => true,
            'maxlength' => 16,
            'validators' => array(
                array(
                    'validator' => 'password',
                    'options' => array(
                        'contextCompare' => true,
                        'inputName' => 'password',
                        'checkLength' => false,
                    )
                )
            ),
        ));

        $this->addSubmitButton('Zmień hasło');
    }

    public function isValid($data){
        $hPassword = $this->getModel()->getAuth()->saltPassword($data['old_password']);
        if($this->getModel()->user()->getPassword() != $hPassword){
            $this->getElement('old_password')->addError('Stare hasło jest nieparwidłowe!');
            return false;
        }
        return parent::isValid($data);
    }
}