<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Basia Choroby
 * Dnia: 2015-04-21 19:02
 */

class Panel_Form_User_Edit extends CMS_Form_Horizontal
{
    /**
     * @var Model_User
     */
    protected $user;
    public function __construct(Model_User $user){
        $this->user = $user;
        parent::__construct();
    }

    public function init(){
        $this->setMethod(self::METHOD_POST);

        $this->addElement('email', 'email', array(
            'label' => 'Adres e-mail',
            'placeholder' => 'adres e-mail',
            'required' => true,
            'readonly' => true,
            'value' => $this->user->getEmail()
        ));

        $this->addElement('text', 'first_name', array(
            'label' => 'Imię',
            'required' => false,
            'placeholder' => 'imię',
            'value' => $this->user->findOneData()->getFirstName(),
        ));

        $this->addElement('text', 'last_name', array(
            'label' => 'Nazwisko',
            'required' => false,
            'placeholder' => 'nazwisko',
            'value' => $this->user->findOneData()->getLastName(),
        ));

        $this->addElement('text', 'phone', array(
            'label' => 'Telefon',
            'required' => false,
            'placeholder' => 'telefon',
            'value' => $this->user->findOneData()->getPhone(),
        ));


        $this->addSubmitButton('Zapisz');
    }

}