<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-28 23:33
 *
 * @method Service_ModelRepository getModel()
 */

class Panel_UserController extends CMS_Controller_Action
{
    public function init(){
        if(!$this->getAuth()->hasIdentity()){
            $this->gotoRouteAndExit('user.login');
        }
    }

    public function indexAction(){
        $this->view->user = $this->getModel()->user();
    }

    public function editAction(){
        $user = $this->getModel()->user();
        if(!$user->findOneData()){
            $data = $user->createData();
            $data->save();
        }
        $form = new Panel_Form_User_Edit($user);
        if($this->isValidPost($form)){
            $data = $user->findOneData();
            $data->setFromArray($form->getValues());
            $data->save();
            $user->setDateModified(CMS_Date::now())->save();
            $this->flashMessage('Zmiany zostały zapisane!');
            $this->gotoRouteAndExit('panel.user.info');
        }
        $this->view->form = $form;
    }

    public function changePasswordAction(){
        $form = new Panel_Form_User_ChangePassword();
        $form->setModel($this->getModel());
        if($this->isValidPost($form)){
            $user = $this->getModel()->user();
            /* @var $user Model_User */
            $user->setPassword($this->getAuth()->saltPassword($form->getValue('password')))->save();
            $this->flashMessage('Hasło zostało zmienione!');
            $this->gotoRouteAndExit('panel.user.info');
        }
        $this->view->form = $form;
    }
}