<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-28 23:30
 */

class Panel_IndexController extends CMS_Controller_Action
{
    public function init(){
        if(!$this->getAuth()->hasIdentity()){
            $this->gotoRouteAndExit('user.login');
        }
    }

    public function indexAction(){
        $user = $this->getModel()->user();
        $this->view->user = $user;
    }
}