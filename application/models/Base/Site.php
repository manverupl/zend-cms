<?php

/**
 * Model_Base_Site
 * *DO NOT* edit this file.
 *
 * This class has been generated automatically by Jack's Model Generator.
 *
 * @property int $id int
 * @property string $slug varchar
 * @property string $title varchar
 * @property string $column_one longtext
 * @property string $column_two longtext
 * @property string $date_created datetime
 * @property string $date_modified datetime
 * @method Model_DbTable_Site getTable()
 * @method Service_ModelRepository getModel()
 * @package Cantor
 * @subpackage Model
 */
class Model_Base_Site extends Zend_Db_Table_Row_Abstract
{

    /**
     * Set id.
     *
     * @param int $value
     * @return Model_Site
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get id.
     *
     * @param mixed $default Optional default value
     * @return int
     */
    public function getId($default = null)
    {
        if($default !== null && ((!isset($this->id))
            || $this->id === '' || $this->id === NULL ))
            return $default;
        return $this->id;
    }

    /**
     * Set slug.
     *
     * @param string $value
     * @return Model_Site
     */
    public function setSlug($value)
    {
        $this->slug = $value;
        return $this;
    }

    /**
     * Get slug.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getSlug($default = null)
    {
        if($default !== null && ((!isset($this->slug))
            || $this->slug === '' || $this->slug === NULL ))
            return $default;
        return $this->slug;
    }

    /**
     * Set title.
     *
     * @param string $value
     * @return Model_Site
     */
    public function setTitle($value)
    {
        $this->title = $value;
        return $this;
    }

    /**
     * Get title.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getTitle($default = null)
    {
        if($default !== null && ((!isset($this->title))
            || $this->title === '' || $this->title === NULL ))
            return $default;
        return $this->title;
    }

    /**
     * Set column_one.
     *
     * @param string $value
     * @return Model_Site
     */
    public function setColumnOne($value)
    {
        $this->column_one = $value;
        return $this;
    }

    /**
     * Get column_one.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getColumnOne($default = null)
    {
        if($default !== null && ((!isset($this->column_one))
            || $this->column_one === '' || $this->column_one === NULL ))
            return $default;
        return $this->column_one;
    }

    /**
     * Set column_two.
     *
     * @param string $value
     * @return Model_Site
     */
    public function setColumnTwo($value)
    {
        $this->column_two = $value;
        return $this;
    }

    /**
     * Get column_two.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getColumnTwo($default = null)
    {
        if($default !== null && ((!isset($this->column_two))
            || $this->column_two === '' || $this->column_two === NULL ))
            return $default;
        return $this->column_two;
    }

    /**
     * Set date_created.
     *
     * @param CMS_Date|string $value
     * @return Model_Site
     */
    public function setDateCreated($value)
    {
        if($value instanceof CMS_Date)
            $value = $value->toMysqlDate();
        $this->date_created = $value;
        return $this;
    }

    /**
     * Get date_created.
     *
     * @param mixed $default Optional default value
     * @return CMS_Date|Zend_Date|string
     */
    public function getDateCreated($default = null)
    {
        if($default !== null && ((!isset($this->date_created))
            || $this->date_created === '' || $this->date_created === NULL ))
            return $default;
        return ($this->date_created != null) ? new CMS_Date($this->date_created) : null;
    }

    /**
     * Set date_modified.
     *
     * @param CMS_Date|string $value
     * @return Model_Site
     */
    public function setDateModified($value)
    {
        if($value instanceof CMS_Date)
            $value = $value->toMysqlDate();
        $this->date_modified = $value;
        return $this;
    }

    /**
     * Get date_modified.
     *
     * @param mixed $default Optional default value
     * @return CMS_Date|Zend_Date|string
     */
    public function getDateModified($default = null)
    {
        if($default !== null && ((!isset($this->date_modified))
            || $this->date_modified === '' || $this->date_modified === NULL ))
            return $default;
        return ($this->date_modified != null) ? new CMS_Date($this->date_modified) : null;
    }


}

