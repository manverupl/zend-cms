<?php

/**
 * Model_Base_News
 * *DO NOT* edit this file.
 *
 * This class has been generated automatically by Jack's Model Generator.
 *
 * @property int $id int
 * @property string $title varchar
 * @property string $author varchar
 * @property string $short_text longtext
 * @property string $text longtext
 * @property string $date_created datetime
 * @property string $date_modified datetime
 * @method Model_DbTable_News getTable()
 * @method Service_ModelRepository getModel()
 * @package Cantor
 * @subpackage Model
 */
class Model_Base_News extends Zend_Db_Table_Row_Abstract
{

    /**
     * Set id.
     *
     * @param int $value
     * @return Model_News
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get id.
     *
     * @param mixed $default Optional default value
     * @return int
     */
    public function getId($default = null)
    {
        if($default !== null && ((!isset($this->id))
            || $this->id === '' || $this->id === NULL ))
            return $default;
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $value
     * @return Model_News
     */
    public function setTitle($value)
    {
        $this->title = $value;
        return $this;
    }

    /**
     * Get title.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getTitle($default = null)
    {
        if($default !== null && ((!isset($this->title))
            || $this->title === '' || $this->title === NULL ))
            return $default;
        return $this->title;
    }

    /**
     * Set author.
     *
     * @param string $value
     * @return Model_News
     */
    public function setAuthor($value)
    {
        $this->author = $value;
        return $this;
    }

    /**
     * Get author.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getAuthor($default = null)
    {
        if($default !== null && ((!isset($this->author))
            || $this->author === '' || $this->author === NULL ))
            return $default;
        return $this->author;
    }

    /**
     * Set short_text.
     *
     * @param string $value
     * @return Model_News
     */
    public function setShortText($value)
    {
        $this->short_text = $value;
        return $this;
    }

    /**
     * Get short_text.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getShortText($default = null)
    {
        if($default !== null && ((!isset($this->short_text))
            || $this->short_text === '' || $this->short_text === NULL ))
            return $default;
        return $this->short_text;
    }

    /**
     * Set text.
     *
     * @param string $value
     * @return Model_News
     */
    public function setText($value)
    {
        $this->text = $value;
        return $this;
    }

    /**
     * Get text.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getText($default = null)
    {
        if($default !== null && ((!isset($this->text))
            || $this->text === '' || $this->text === NULL ))
            return $default;
        return $this->text;
    }

    /**
     * Set date_created.
     *
     * @param CMS_Date|string $value
     * @return Model_News
     */
    public function setDateCreated($value)
    {
        if($value instanceof CMS_Date)
            $value = $value->toMysqlDate();
        $this->date_created = $value;
        return $this;
    }

    /**
     * Get date_created.
     *
     * @param mixed $default Optional default value
     * @return CMS_Date|Zend_Date|string
     */
    public function getDateCreated($default = null)
    {
        if($default !== null && ((!isset($this->date_created))
            || $this->date_created === '' || $this->date_created === NULL ))
            return $default;
        return ($this->date_created != null) ? new CMS_Date($this->date_created) : null;
    }

    /**
     * Set date_modified.
     *
     * @param CMS_Date|string $value
     * @return Model_News
     */
    public function setDateModified($value)
    {
        if($value instanceof CMS_Date)
            $value = $value->toMysqlDate();
        $this->date_modified = $value;
        return $this;
    }

    /**
     * Get date_modified.
     *
     * @param mixed $default Optional default value
     * @return CMS_Date|Zend_Date|string
     */
    public function getDateModified($default = null)
    {
        if($default !== null && ((!isset($this->date_modified))
            || $this->date_modified === '' || $this->date_modified === NULL ))
            return $default;
        return ($this->date_modified != null) ? new CMS_Date($this->date_modified) : null;
    }


}

