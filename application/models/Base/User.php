<?php

/**
 * Model_Base_User
 * *DO NOT* edit this file.
 *
 * This class has been generated automatically by Jack's Model Generator.
 *
 * @property int $id int
 * @property string $email varchar
 * @property string $password varchar
 * @property string $role enum('superadmin','admin','user','guest')
 * @property string $date_registered datetime
 * @property string $date_last_login datetime
 * @property string $date_last_login_fail datetime
 * @property string $date_modified datetime
 * @property int $enabled tinyint
 * @property int $active tinyint
 * @method Model_DbTable_User getTable()
 * @method Service_ModelRepository getModel()
 * @package Cantor
 * @subpackage Model
 */
class Model_Base_User extends Zend_Db_Table_Row_Abstract
{

    const ROLE_SUPERADMIN = 'superadmin';

    const ROLE_ADMIN = 'admin';

    const ROLE_USER = 'user';

    const ROLE_GUEST = 'guest';

    /**
     * Set id.
     *
     * @param int $value
     * @return Model_User
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get id.
     *
     * @param mixed $default Optional default value
     * @return int
     */
    public function getId($default = null)
    {
        if($default !== null && ((!isset($this->id))
            || $this->id === '' || $this->id === NULL ))
            return $default;
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string $value
     * @return Model_User
     */
    public function setEmail($value)
    {
        $this->email = $value;
        return $this;
    }

    /**
     * Get email.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getEmail($default = null)
    {
        if($default !== null && ((!isset($this->email))
            || $this->email === '' || $this->email === NULL ))
            return $default;
        return $this->email;
    }

    /**
     * Set password.
     *
     * @param string $value
     * @return Model_User
     */
    public function setPassword($value)
    {
        $this->password = $value;
        return $this;
    }

    /**
     * Get password.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getPassword($default = null)
    {
        if($default !== null && ((!isset($this->password))
            || $this->password === '' || $this->password === NULL ))
            return $default;
        return $this->password;
    }

    /**
     * Set role.
     *
     * @param string $value
     * @return Model_User
     */
    public function setRole($value)
    {
        $this->role = $value;
        return $this;
    }

    /**
     * Get role.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getRole($default = null)
    {
        if($default !== null && ((!isset($this->role))
            || $this->role === '' || $this->role === NULL ))
            return $default;
        return $this->role;
    }

    /**
     * Set role to superadmin
     *
     * @return Model_User
     */
    public function setRoleSuperadmin()
    {
        return $this->setRole(self::ROLE_SUPERADMIN);
    }

    /**
     * Is role set to superadmin ?
     *
     * @return bool
     */
    public function isRoleSuperadmin()
    {
        return $this->getRole() == self::ROLE_SUPERADMIN;
    }

    /**
     * Is role set to superadmin ?
     *
     * @return bool
     */
    public function getIsRoleSuperadmin()
    {
        return $this->isRoleSuperadmin();
    }

    /**
     * Set role to admin
     *
     * @return Model_User
     */
    public function setRoleAdmin()
    {
        return $this->setRole(self::ROLE_ADMIN);
    }

    /**
     * Is role set to admin ?
     *
     * @return bool
     */
    public function isRoleAdmin()
    {
        return $this->getRole() == self::ROLE_ADMIN;
    }

    /**
     * Is role set to admin ?
     *
     * @return bool
     */
    public function getIsRoleAdmin()
    {
        return $this->isRoleAdmin();
    }

    /**
     * Set role to user
     *
     * @return Model_User
     */
    public function setRoleUser()
    {
        return $this->setRole(self::ROLE_USER);
    }

    /**
     * Is role set to user ?
     *
     * @return bool
     */
    public function isRoleUser()
    {
        return $this->getRole() == self::ROLE_USER;
    }

    /**
     * Is role set to user ?
     *
     * @return bool
     */
    public function getIsRoleUser()
    {
        return $this->isRoleUser();
    }

    /**
     * Set role to guest
     *
     * @return Model_User
     */
    public function setRoleGuest()
    {
        return $this->setRole(self::ROLE_GUEST);
    }

    /**
     * Is role set to guest ?
     *
     * @return bool
     */
    public function isRoleGuest()
    {
        return $this->getRole() == self::ROLE_GUEST;
    }

    /**
     * Is role set to guest ?
     *
     * @return bool
     */
    public function getIsRoleGuest()
    {
        return $this->isRoleGuest();
    }

    /**
     * Get value list for "role" property.
     *
     * @param string $labelPrefix
     * @return array[superadmin, admin, user, guest]
     */
    public static function getRoleList($labelPrefix = null)
    {
        $a = array(
                'superadmin' => 'superadmin',
                'admin' => 'admin',
                'user' => 'user',
                'guest' => 'guest'
                );
        if(!empty($labelPrefix) || $labelPrefix == 0)
            foreach($a as &$b) $b = $labelPrefix.$b;
        return $a;
    }

    /**
     * Set date_registered.
     *
     * @param CMS_Date|string $value
     * @return Model_User
     */
    public function setDateRegistered($value)
    {
        if($value instanceof CMS_Date)
            $value = $value->toMysqlDate();
        $this->date_registered = $value;
        return $this;
    }

    /**
     * Get date_registered.
     *
     * @param mixed $default Optional default value
     * @return CMS_Date|Zend_Date|string
     */
    public function getDateRegistered($default = null)
    {
        if($default !== null && ((!isset($this->date_registered))
            || $this->date_registered === '' || $this->date_registered === NULL ))
            return $default;
        return ($this->date_registered != null) ? new CMS_Date($this->date_registered) : null;
    }

    /**
     * Set date_last_login.
     *
     * @param CMS_Date|string $value
     * @return Model_User
     */
    public function setDateLastLogin($value)
    {
        if($value instanceof CMS_Date)
            $value = $value->toMysqlDate();
        $this->date_last_login = $value;
        return $this;
    }

    /**
     * Get date_last_login.
     *
     * @param mixed $default Optional default value
     * @return CMS_Date|Zend_Date|string
     */
    public function getDateLastLogin($default = null)
    {
        if($default !== null && ((!isset($this->date_last_login))
            || $this->date_last_login === '' || $this->date_last_login === NULL ))
            return $default;
        return ($this->date_last_login != null) ? new CMS_Date($this->date_last_login) : null;
    }

    /**
     * Set date_last_login_fail.
     *
     * @param CMS_Date|string $value
     * @return Model_User
     */
    public function setDateLastLoginFail($value)
    {
        if($value instanceof CMS_Date)
            $value = $value->toMysqlDate();
        $this->date_last_login_fail = $value;
        return $this;
    }

    /**
     * Get date_last_login_fail.
     *
     * @param mixed $default Optional default value
     * @return CMS_Date|Zend_Date|string
     */
    public function getDateLastLoginFail($default = null)
    {
        if($default !== null && ((!isset($this->date_last_login_fail))
            || $this->date_last_login_fail === '' || $this->date_last_login_fail === NULL ))
            return $default;
        return ($this->date_last_login_fail != null) ? new CMS_Date($this->date_last_login_fail) : null;
    }

    /**
     * Set date_modified.
     *
     * @param CMS_Date|string $value
     * @return Model_User
     */
    public function setDateModified($value)
    {
        if($value instanceof CMS_Date)
            $value = $value->toMysqlDate();
        $this->date_modified = $value;
        return $this;
    }

    /**
     * Get date_modified.
     *
     * @param mixed $default Optional default value
     * @return CMS_Date|Zend_Date|string
     */
    public function getDateModified($default = null)
    {
        if($default !== null && ((!isset($this->date_modified))
            || $this->date_modified === '' || $this->date_modified === NULL ))
            return $default;
        return ($this->date_modified != null) ? new CMS_Date($this->date_modified) : null;
    }

    /**
     * Set enabled.
     *
     * @param int $value
     * @return Model_User
     */
    public function setEnabled($value)
    {
        $this->enabled = $value;
        return $this;
    }

    /**
     * Get enabled.
     *
     * @param mixed $default Optional default value
     * @return int
     */
    public function getEnabled($default = null)
    {
        if($default !== null && ((!isset($this->enabled))
            || $this->enabled === '' || $this->enabled === NULL ))
            return $default;
        return $this->enabled;
    }

    /**
     * Set active.
     *
     * @param int $value
     * @return Model_User
     */
    public function setActive($value)
    {
        $this->active = $value;
        return $this;
    }

    /**
     * Get active.
     *
     * @param mixed $default Optional default value
     * @return int
     */
    public function getActive($default = null)
    {
        if($default !== null && ((!isset($this->active))
            || $this->active === '' || $this->active === NULL ))
            return $default;
        return $this->active;
    }

    /**
     * Create new Model_UserData
     *
     * @param array $data
     * @return Model_UserData
     */
    public function createData($data = array ())
    {
        $data['user_id'] = $this->id;
        $table = new Model_DbTable_UserData();
        $row = $table->createRow($data);
        return $row;
    }

    /**
     * Find dependent Model_UserData
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $order
     * @param int $count
     * @param int $offset
     * @return Zend_Db_Table_Rowset_Abstract|Model_UserData[]
     */
    public function findData($where = null, $order = null, $count = null, $offset = null)
    {
        return $this->findDependentRowset('Model_DbTable_UserData', $where, $order, $count, $offset, 'user_data_ibfk_1');
    }

    /**
     * Find one dependent Model_UserData
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $order
     * @param int $offset
     * @return Model_UserData
     */
    public function findOneData($where = null, $order = null, $offset = null)
    {
        return $this->findDependentRowset('Model_DbTable_UserData', $where, $order, 1, $offset, 'user_data_ibfk_1')->current();
    }

    /**
     * Delete all dependant Model_UserData matching condition.
     *
     * @param array $where
     * @return int Number of deleted rows
     */
    public function deleteData($where = array())
    {
        $where['user_id = ?'] = $this->id;
        $table = new Model_DbTable_UserData();
        return $table->delete($where);
    }


}

