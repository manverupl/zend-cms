<?php

/**
 * Model_DbTable_UserData
 * *DO NOT* edit this file.
 *
 * This class has been generated automatically by Jack's Model Generator.
 *
 * @package Cantor
 * @subpackage Model
 * @method Model_UserData createRow(array $data = null)
 * @method static Service_ModelRepository getModel()
 */
class Model_Base_DbTable_UserData extends CMS_Model_DbTable_Abstract
{

    protected $_name = 'user_data';

    protected $_primary = array('id');

    protected $_rowClass = 'Model_UserData';

    protected $_referenceMap = array('user_data_ibfk_1' => array(
            'columns' => array('user_id'),
            'refTableClass' => 'Model_DbTable_User',
            'refColumns' => array('id')
            ));

    protected $_dependantTables = array();

    protected $_metadata = array(
        'id' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user_data',
            'COLUMN_NAME' => 'id',
            'COLUMN_POSITION' => 1,
            'DATA_TYPE' => 'int',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => true,
            'PRIMARY_POSITION' => 1,
            'IDENTITY' => true
            ),
        'user_id' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user_data',
            'COLUMN_NAME' => 'user_id',
            'COLUMN_POSITION' => 2,
            'DATA_TYPE' => 'int',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'first_name' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user_data',
            'COLUMN_NAME' => 'first_name',
            'COLUMN_POSITION' => 3,
            'DATA_TYPE' => 'varchar',
            'DEFAULT' => null,
            'NULLABLE' => true,
            'LENGTH' => '150',
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'last_name' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user_data',
            'COLUMN_NAME' => 'last_name',
            'COLUMN_POSITION' => 4,
            'DATA_TYPE' => 'varchar',
            'DEFAULT' => null,
            'NULLABLE' => true,
            'LENGTH' => '150',
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'phone' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user_data',
            'COLUMN_NAME' => 'phone',
            'COLUMN_POSITION' => 5,
            'DATA_TYPE' => 'varchar',
            'DEFAULT' => null,
            'NULLABLE' => true,
            'LENGTH' => '15',
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            )
        );

    /**
     * Find all rows from Model_DbTable_UserData matching criteria.
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $orderBy
     * @param int $count
     * @param int $offset
     * @param string $group
     * @return Zend_Db_Table_Rowset_Abstract|Model_UserData[]
     */
    public function findAll($where = null, $orderBy = null, $count = null, $offset = null, $group = null)
    {
        return parent::findAll($where, $orderBy, $count, $offset, $group);
    }

    /**
     * Find all rows from Model_DbTable_UserData matching criteria.
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $orderBy
     * @return Model_UserData
     */
    public function findOne($where = null, $orderBy = null)
    {
        return parent::findOne($where, $orderBy);
    }

    /**
     * Find row by id.
     *
     * @param int $value
     * @return Model_UserData
     */
    public function findById($value)
    {
        return parent::findOne(array('`id` = ?' => (int) $value));
    }


}

