<?php

/**
 * Model_DbTable_Site
 * *DO NOT* edit this file.
 *
 * This class has been generated automatically by Jack's Model Generator.
 *
 * @package Cantor
 * @subpackage Model
 * @method Model_Site createRow(array $data = null)
 * @method static Service_ModelRepository getModel()
 */
class Model_Base_DbTable_Site extends CMS_Model_DbTable_Abstract
{

    protected $_name = 'site';

    protected $_primary = array('id');

    protected $_rowClass = 'Model_Site';

    protected $_referenceMap = array();

    protected $_dependantTables = array();

    protected $_metadata = array(
        'id' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'site',
            'COLUMN_NAME' => 'id',
            'COLUMN_POSITION' => 1,
            'DATA_TYPE' => 'int',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => true,
            'PRIMARY_POSITION' => 1,
            'IDENTITY' => true
            ),
        'slug' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'site',
            'COLUMN_NAME' => 'slug',
            'COLUMN_POSITION' => 2,
            'DATA_TYPE' => 'varchar',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => '50',
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'title' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'site',
            'COLUMN_NAME' => 'title',
            'COLUMN_POSITION' => 3,
            'DATA_TYPE' => 'varchar',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => '200',
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'column_one' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'site',
            'COLUMN_NAME' => 'column_one',
            'COLUMN_POSITION' => 4,
            'DATA_TYPE' => 'longtext',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'column_two' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'site',
            'COLUMN_NAME' => 'column_two',
            'COLUMN_POSITION' => 5,
            'DATA_TYPE' => 'longtext',
            'DEFAULT' => null,
            'NULLABLE' => true,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'date_created' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'site',
            'COLUMN_NAME' => 'date_created',
            'COLUMN_POSITION' => 6,
            'DATA_TYPE' => 'datetime',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'date_modified' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'site',
            'COLUMN_NAME' => 'date_modified',
            'COLUMN_POSITION' => 7,
            'DATA_TYPE' => 'datetime',
            'DEFAULT' => null,
            'NULLABLE' => true,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            )
        );

    /**
     * Find all rows from Model_DbTable_Site matching criteria.
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $orderBy
     * @param int $count
     * @param int $offset
     * @param string $group
     * @return Zend_Db_Table_Rowset_Abstract|Model_Site[]
     */
    public function findAll($where = null, $orderBy = null, $count = null, $offset = null, $group = null)
    {
        return parent::findAll($where, $orderBy, $count, $offset, $group);
    }

    /**
     * Find all rows from Model_DbTable_Site matching criteria.
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $orderBy
     * @return Model_Site
     */
    public function findOne($where = null, $orderBy = null)
    {
        return parent::findOne($where, $orderBy);
    }

    /**
     * Find row by id.
     *
     * @param int $value
     * @return Model_Site
     */
    public function findById($value)
    {
        return parent::findOne(array('`id` = ?' => (int) $value));
    }

    /**
     * Find row by slug.
     *
     * @param string $value
     * @return Model_Site
     */
    public function findBySlug($value)
    {
        return parent::findOne(array('`slug` = ?' => (string) $value));
    }


}

