<?php

/**
 * Model_DbTable_Settings
 * *DO NOT* edit this file.
 *
 * This class has been generated automatically by Jack's Model Generator.
 *
 * @package Cantor
 * @subpackage Model
 * @method Model_Settings createRow(array $data = null)
 * @method static Service_ModelRepository getModel()
 */
class Model_Base_DbTable_Settings extends CMS_Model_DbTable_Abstract
{

    protected $_name = 'settings';

    protected $_primary = array('id');

    protected $_rowClass = 'Model_Settings';

    protected $_referenceMap = array();

    protected $_dependantTables = array();

    protected $_metadata = array(
        'id' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'settings',
            'COLUMN_NAME' => 'id',
            'COLUMN_POSITION' => 1,
            'DATA_TYPE' => 'int',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => true,
            'PRIMARY_POSITION' => 1,
            'IDENTITY' => true
            ),
        'name' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'settings',
            'COLUMN_NAME' => 'name',
            'COLUMN_POSITION' => 2,
            'DATA_TYPE' => 'varchar',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => '50',
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'value' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'settings',
            'COLUMN_NAME' => 'value',
            'COLUMN_POSITION' => 3,
            'DATA_TYPE' => 'varchar',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => '200',
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'date_modified' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'settings',
            'COLUMN_NAME' => 'date_modified',
            'COLUMN_POSITION' => 4,
            'DATA_TYPE' => 'datetime',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            )
        );

    /**
     * Find all rows from Model_DbTable_Settings matching criteria.
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $orderBy
     * @param int $count
     * @param int $offset
     * @param string $group
     * @return Zend_Db_Table_Rowset_Abstract|Model_Settings[]
     */
    public function findAll($where = null, $orderBy = null, $count = null, $offset = null, $group = null)
    {
        return parent::findAll($where, $orderBy, $count, $offset, $group);
    }

    /**
     * Find all rows from Model_DbTable_Settings matching criteria.
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $orderBy
     * @return Model_Settings
     */
    public function findOne($where = null, $orderBy = null)
    {
        return parent::findOne($where, $orderBy);
    }

    /**
     * Find row by id.
     *
     * @param int $value
     * @return Model_Settings
     */
    public function findById($value)
    {
        return parent::findOne(array('`id` = ?' => (int) $value));
    }

    /**
     * Find row by name.
     *
     * @param string $value
     * @return Model_Settings
     */
    public function findByName($value)
    {
        return parent::findOne(array('`name` = ?' => (string) $value));
    }


}

