<?php

/**
 * Model_DbTable_User
 * *DO NOT* edit this file.
 *
 * This class has been generated automatically by Jack's Model Generator.
 *
 * @package Cantor
 * @subpackage Model
 * @method Model_User createRow(array $data = null)
 * @method static Service_ModelRepository getModel()
 */
class Model_Base_DbTable_User extends CMS_Model_DbTable_Abstract
{

    protected $_name = 'user';

    protected $_primary = array('id');

    protected $_rowClass = 'Model_User';

    protected $_referenceMap = array();

    protected $_dependantTables = array('Model_DbTable_UserData');

    protected $_metadata = array(
        'id' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user',
            'COLUMN_NAME' => 'id',
            'COLUMN_POSITION' => 1,
            'DATA_TYPE' => 'int',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => true,
            'PRIMARY_POSITION' => 1,
            'IDENTITY' => true
            ),
        'email' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user',
            'COLUMN_NAME' => 'email',
            'COLUMN_POSITION' => 2,
            'DATA_TYPE' => 'varchar',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => '50',
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'password' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user',
            'COLUMN_NAME' => 'password',
            'COLUMN_POSITION' => 3,
            'DATA_TYPE' => 'varchar',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => '250',
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'role' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user',
            'COLUMN_NAME' => 'role',
            'COLUMN_POSITION' => 4,
            'DATA_TYPE' => 'enum(\'superadmin\',\'admin\',\'user\',\'guest\')',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'date_registered' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user',
            'COLUMN_NAME' => 'date_registered',
            'COLUMN_POSITION' => 5,
            'DATA_TYPE' => 'datetime',
            'DEFAULT' => null,
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'date_last_login' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user',
            'COLUMN_NAME' => 'date_last_login',
            'COLUMN_POSITION' => 6,
            'DATA_TYPE' => 'datetime',
            'DEFAULT' => null,
            'NULLABLE' => true,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'date_last_login_fail' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user',
            'COLUMN_NAME' => 'date_last_login_fail',
            'COLUMN_POSITION' => 7,
            'DATA_TYPE' => 'datetime',
            'DEFAULT' => null,
            'NULLABLE' => true,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'date_modified' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user',
            'COLUMN_NAME' => 'date_modified',
            'COLUMN_POSITION' => 8,
            'DATA_TYPE' => 'datetime',
            'DEFAULT' => null,
            'NULLABLE' => true,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'enabled' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user',
            'COLUMN_NAME' => 'enabled',
            'COLUMN_POSITION' => 9,
            'DATA_TYPE' => 'tinyint',
            'DEFAULT' => '1',
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            ),
        'active' => array(
            'SCHEMA_NAME' => null,
            'TABLE_NAME' => 'user',
            'COLUMN_NAME' => 'active',
            'COLUMN_POSITION' => 10,
            'DATA_TYPE' => 'tinyint',
            'DEFAULT' => '0',
            'NULLABLE' => false,
            'LENGTH' => null,
            'SCALE' => null,
            'PRECISION' => null,
            'UNSIGNED' => null,
            'PRIMARY' => false,
            'PRIMARY_POSITION' => null,
            'IDENTITY' => false
            )
        );

    /**
     * Find all rows from Model_DbTable_User matching criteria.
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $orderBy
     * @param int $count
     * @param int $offset
     * @param string $group
     * @return Zend_Db_Table_Rowset_Abstract|Model_User[]
     */
    public function findAll($where = null, $orderBy = null, $count = null, $offset = null, $group = null)
    {
        return parent::findAll($where, $orderBy, $count, $offset, $group);
    }

    /**
     * Find all rows from Model_DbTable_User matching criteria.
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $orderBy
     * @return Model_User
     */
    public function findOne($where = null, $orderBy = null)
    {
        return parent::findOne($where, $orderBy);
    }

    /**
     * Find row by id.
     *
     * @param int $value
     * @return Model_User
     */
    public function findById($value)
    {
        return parent::findOne(array('`id` = ?' => (int) $value));
    }

    /**
     * Find row by email.
     *
     * @param string $value
     * @return Model_User
     */
    public function findByEmail($value)
    {
        return parent::findOne(array('`email` = ?' => (string) $value));
    }


}

