<?php

/**
 * Model_Base_Settings
 * *DO NOT* edit this file.
 *
 * This class has been generated automatically by Jack's Model Generator.
 *
 * @property int $id int
 * @property string $name varchar
 * @property string $value varchar
 * @property string $date_modified datetime
 * @method Model_DbTable_Settings getTable()
 * @method Service_ModelRepository getModel()
 * @package Cantor
 * @subpackage Model
 */
class Model_Base_Settings extends Zend_Db_Table_Row_Abstract
{

    /**
     * Set id.
     *
     * @param int $value
     * @return Model_Settings
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get id.
     *
     * @param mixed $default Optional default value
     * @return int
     */
    public function getId($default = null)
    {
        if($default !== null && ((!isset($this->id))
            || $this->id === '' || $this->id === NULL ))
            return $default;
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $value
     * @return Model_Settings
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * Get name.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getName($default = null)
    {
        if($default !== null && ((!isset($this->name))
            || $this->name === '' || $this->name === NULL ))
            return $default;
        return $this->name;
    }

    /**
     * Set value.
     *
     * @param string $value
     * @return Model_Settings
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getValue($default = null)
    {
        if($default !== null && ((!isset($this->value))
            || $this->value === '' || $this->value === NULL ))
            return $default;
        return $this->value;
    }

    /**
     * Set date_modified.
     *
     * @param CMS_Date|string $value
     * @return Model_Settings
     */
    public function setDateModified($value)
    {
        if($value instanceof CMS_Date)
            $value = $value->toMysqlDate();
        $this->date_modified = $value;
        return $this;
    }

    /**
     * Get date_modified.
     *
     * @param mixed $default Optional default value
     * @return CMS_Date|Zend_Date|string
     */
    public function getDateModified($default = null)
    {
        if($default !== null && ((!isset($this->date_modified))
            || $this->date_modified === '' || $this->date_modified === NULL ))
            return $default;
        return ($this->date_modified != null) ? new CMS_Date($this->date_modified) : null;
    }


}

