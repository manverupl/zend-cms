<?php

/**
 * Model_Base_UserData
 * *DO NOT* edit this file.
 *
 * This class has been generated automatically by Jack's Model Generator.
 *
 * @property int $id int
 * @property int $user_id int
 * @property string $first_name varchar
 * @property string $last_name varchar
 * @property string $phone varchar
 * @method Model_DbTable_UserData getTable()
 * @method Service_ModelRepository getModel()
 * @package Cantor
 * @subpackage Model
 */
class Model_Base_UserData extends Zend_Db_Table_Row_Abstract
{

    /**
     * Set id.
     *
     * @param int $value
     * @return Model_UserData
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get id.
     *
     * @param mixed $default Optional default value
     * @return int
     */
    public function getId($default = null)
    {
        if($default !== null && ((!isset($this->id))
            || $this->id === '' || $this->id === NULL ))
            return $default;
        return $this->id;
    }

    /**
     * Set user_id.
     *
     * @param int $value
     * @return Model_UserData
     */
    public function setUserId($value)
    {
        $this->user_id = $value;
        return $this;
    }

    /**
     * Get user_id.
     *
     * @param mixed $default Optional default value
     * @return int
     */
    public function getUserId($default = null)
    {
        if($default !== null && ((!isset($this->user_id))
            || $this->user_id === '' || $this->user_id === NULL ))
            return $default;
        return $this->user_id;
    }

    /**
     * Set first_name.
     *
     * @param string $value
     * @return Model_UserData
     */
    public function setFirstName($value)
    {
        $this->first_name = $value;
        return $this;
    }

    /**
     * Get first_name.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getFirstName($default = null)
    {
        if($default !== null && ((!isset($this->first_name))
            || $this->first_name === '' || $this->first_name === NULL ))
            return $default;
        return $this->first_name;
    }

    /**
     * Set last_name.
     *
     * @param string $value
     * @return Model_UserData
     */
    public function setLastName($value)
    {
        $this->last_name = $value;
        return $this;
    }

    /**
     * Get last_name.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getLastName($default = null)
    {
        if($default !== null && ((!isset($this->last_name))
            || $this->last_name === '' || $this->last_name === NULL ))
            return $default;
        return $this->last_name;
    }

    /**
     * Set phone.
     *
     * @param string $value
     * @return Model_UserData
     */
    public function setPhone($value)
    {
        $this->phone = $value;
        return $this;
    }

    /**
     * Get phone.
     *
     * @param mixed $default Optional default value
     * @return string
     */
    public function getPhone($default = null)
    {
        if($default !== null && ((!isset($this->phone))
            || $this->phone === '' || $this->phone === NULL ))
            return $default;
        return $this->phone;
    }

    /**
     * Find parent Model_User
     *
     * @param string|array|Zend_Db_Table_Select $where
     * @param string|array $order
     * @param int $count
     * @param int $offset
     * @return Model_User
     */
    public function findUser($where = null, $order = null, $count = null, $offset = null)
    {
        return $this->findParentRow('Model_DbTable_User', $where, $order, $count, $offset, 'user_data_ibfk_1');
    }

    /**
     * Create new parent Model_User and SAVE IT.
     *
     * @param array $data
     * @deprecated This method is a stub.
     * @return Model_User
     */
    public function createParentUser($data = array ())
    {
        $table = new Model_DbTable_User($this->getTable()->getAdapter());
        $row = $table->createRow($data);
        $row->save();
        $this->user_id = $row->id;
        return $row;
    }


}

