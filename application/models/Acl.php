<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: HCSerwis v2.0
 * Dnia: 2015-03-25 20:05
 */

class Model_Acl extends Model_Acl_Abstract
{
    protected function initResources(){
        $this->addResource('landing.index.index', self::RESOURCE_EVERYBODY);
        $this->addResource('landing.index.about', self::RESOURCE_EVERYBODY);
        $this->addResource('landing.index.contact', self::RESOURCE_EVERYBODY);
        $this->addResource('landing.index.site', self::RESOURCE_EVERYBODY);
        $this->addResource('landing.news.index', self::RESOURCE_EVERYBODY);
        $this->addResource('landing.news.news', self::RESOURCE_EVERYBODY);
        $this->addResource('landing.auth.login', self::RESOURCE_EVERYBODY);
        $this->addResource('landing.auth.register', self::RESOURCE_EVERYBODY);
        $this->addResource('landing.auth.logout', self::RESOURCE_USERS);
        $this->addResource('panel.index.index', self::RESOURCE_USERS);
        $this->addResource('panel.user.index', self::RESOURCE_USERS);
        $this->addResource('panel.user.edit', self::RESOURCE_USERS);
        $this->addResource('panel.user.change-password', self::RESOURCE_USERS);
        $this->addResource('admin.index.index', self::RESOURCE_ADMINS);
        $this->addResource('admin.user.index', self::RESOURCE_ADMINS);
        $this->addResource('admin.user.add', self::RESOURCE_ADMINS);
        $this->addResource('admin.user.edit', self::RESOURCE_ADMINS);
        $this->addResource('admin.user.details', self::RESOURCE_ADMINS);
        $this->addResource('admin.landing.index', self::RESOURCE_ADMINS);
        $this->addResource('admin.landing.home', self::RESOURCE_ADMINS);
        $this->addResource('admin.landing.about', self::RESOURCE_ADMINS);
        $this->addResource('admin.landing.contact', self::RESOURCE_ADMINS);
        $this->addResource('admin.landing.site', self::RESOURCE_ADMINS);
        $this->addResource('admin.landing.add', self::RESOURCE_ADMINS);
        $this->addResource('admin.news.index', self::RESOURCE_ADMINS);
        $this->addResource('admin.news.edit', self::RESOURCE_ADMINS);
        $this->addResource('admin.news.add', self::RESOURCE_ADMINS);
    }
    protected function initRules(){

    }
}