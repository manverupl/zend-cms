<?php 

/**
 * Assertions factory
 * @author Jacek Kobus <kobus.jacek@gmail.com>
 * @package Cantor
 * @subpackage Model
 * @category ACL
 */
class Model_Acl_AssertFactory
{
	/**
	 * @var Model_Acl
	 */
	protected $acl;

	/**
	 * @param Model_Acl $acl
	 */
	public function __construct(Model_Acl $acl)
	{
		$this->acl = $acl;
	}

	/**
	 * @return Model_Acl_Assert_IsInvalidMember
	 */
	public function isInvalidMember()
	{
		return new Model_Acl_Assert_IsInvalidMember(
			$this->acl->getModel()->cantor()
		);
	}

	/**
	 * @return Model_Acl_Assert_IsValidMember
	 */
	public function isValidMember()
	{
		return new Model_Acl_Assert_IsValidMember(
			$this->acl->getModel()->cantor()
		);
	}

	public function testDeny()
	{
		return new Model_Acl_Assert_TestDeny();
	}

	public function testAllow()
	{
		return new Model_Acl_Assert_TestAllow();
	}
}