<?php 

/**
 * Return TRUE if member is invalid
 */
class Model_Acl_Assert_IsInvalidMember extends Model_Acl_Assert_IsValidMember
{
	/**
	 * @param Zend_Acl $acl
	 * @param Zend_Acl_Role_Interface $role
	 * @param Zend_Acl_Resource_Interface $resource
	 * @param null $privilege
	 * @return bool
	 */
	public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
	                       Zend_Acl_Resource_Interface $resource = null, $privilege = null)
	{
		return !parent::assert($acl, $role, $resource, $privilege);
	}
}