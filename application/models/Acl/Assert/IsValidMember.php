<?php 

/**
 * Must be active, enabled and have all required address info
 */
class Model_Acl_Assert_IsValidMember implements Zend_Acl_Assert_Interface
{
	/**
	 * @var Model_Cantor
	 */
	private $cantor;

	/**
	 * @param Model_Cantor $cantor
	 */
	public function __construct(Model_Cantor $cantor)
	{
		$this->cantor = $cantor;
	}

	/**
	 * @param Zend_Acl $acl
	 * @param Zend_Acl_Role_Interface $role
	 * @param Zend_Acl_Resource_Interface $resource
	 * @param null $privilege
	 * @return bool
	 */
	public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
	                       Zend_Acl_Resource_Interface $resource = null, $privilege = null)
	{
		if (!$role instanceof Model_User)
			return false;

		if (!$role->isActive() || !$role->isEnabled() || !$role->isPhoneConfirmed())
			return false;

		$allCurAccount = $role->findBankAccount()->count();
		$defCurAccount = $role->findBankAccountByCurrency(
			$this->cantor->getDefaultCurrency()->getCode()
		)->count();

		if ($allCurAccount < 2 || $allCurAccount < $defCurAccount)
			return false;

		return true;
	}
}