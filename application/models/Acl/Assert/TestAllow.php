<?php 

/**
 * Must be active, enabled and have all required address info
 */
class Model_Acl_Assert_TestAllow implements Zend_Acl_Assert_Interface
{

	/**
	 * @param Zend_Acl $acl
	 * @param Zend_Acl_Role_Interface $role
	 * @param Zend_Acl_Resource_Interface $resource
	 * @param null $privilege
	 * @return bool
	 */
	public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
	                       Zend_Acl_Resource_Interface $resource = null, $privilege = null)
	{
		return true;
	}
}