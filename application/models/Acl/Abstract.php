<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: HCSerwis v2.0
 * Dnia: 2015-03-25 20:05
 */

abstract class Model_Acl_Abstract extends CMS_Acl
{
    const RESOURCE_NONE = null;
    const RESOURCE_EVERYBODY = 'public';
    const RESOURCE_USERS = 'users';
    const RESOURCE_ADMINS = 'admins';
    const RESOURCE_SUPERADMINS = 'superadmins';

    const DEFAULT_ROLE = Model_User::ROLE_GUEST;

    const ROLE_GUEST = 'guest';
    const ROLE_USER = Model_User::ROLE_USER;
    const ROLE_ADMIN = Model_User::ROLE_ADMIN;
    const ROLE_SUPERADMIN = Model_User::ROLE_SUPERADMIN;

    protected $model;

    public function __construct(Service_ModelRepository $model)
    {
        $this->model = $model;
        $this->assertions = new Model_Acl_AssertFactory($this);
        $this->init();
    }

    private function init(){
        foreach($this->getDefinedRoles() as $name)
            if(!$this->hasRole($name)) $this->addRole($name);

        $this->addResource(Model_Acl::RESOURCE_EVERYBODY);
        $this->addResource(Model_Acl::RESOURCE_USERS);
        $this->addResource(Model_Acl::RESOURCE_ADMINS);
        $this->addResource(Model_Acl::RESOURCE_SUPERADMINS);

        $this->deny();
        $this->allow(Model_Acl::ROLE_SUPERADMIN, null);
        $this->allow(Model_Acl::ROLE_ADMIN, null);
        $this->deny(Model_Acl::ROLE_ADMIN, Model_Acl::RESOURCE_SUPERADMINS);

        $this->allow(Model_Acl::ROLE_GUEST, Model_Acl::RESOURCE_EVERYBODY);
        $this->allow(Model_Acl::ROLE_USER, Model_Acl::RESOURCE_USERS);
        $this->allow(Model_Acl::ROLE_USER, Model_Acl::RESOURCE_EVERYBODY);

        $this->setDefaultRole(Model_Acl::DEFAULT_ROLE);
        $this->initResources();
        $this->initRules();
        return $this;
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * Return an assertion factory
     * @return Model_Acl_AssertFactory
     */
    public function assert()
    {
        return $this->assertions;
    }

    /**
     * @return array
     */
    private function getDefinedRoles()
    {
        return Model_User::getRoleList();
    }

    /**
     * @return mixed
     */
//	protected abstract function initRoles();

    /**
     * @return mixed
     */
    protected abstract function initResources();

    /**
     * @return mixed
     */
    protected abstract function initRules();
}