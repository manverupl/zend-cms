<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-25 22:26
 *
 * @property CMS_View view
 * @property Zend_Controller_Router_Rewrite router
 * @property Zend_Db_Adapter_Pdo_Abstract db
 * @property CMS_Service_Auth auth
 * @property Model_Acl acl
 * @property CMS_Service_Navigation navigation
 * @property Service_ModelRepository model
 *
 */

class Bootstrap extends CMS_Application_Bootstrap_Bootstrap
{
    public function Settings(){
        Zend_Registry::set('UserCanRegister', true);
    }

    public function _initAutoload(){
        $this->Settings();

        $loader = new Zend_Application_Module_Autoloader(array(
            'namespace' => '',
            'basePath' => APPLICATION_PATH
        ));
        return $loader;
    }
    public function _initPlugins(){
        $this->getFrontController()->registerPlugin(new Plugin_LayoutSwitcher(), -100);
    }

    public function _initSession(){
        Zend_Session::start();
    }

    public function _initRoutes(){
        $router = $this->getFrontController()->getRouter();
        /* @var $router Zend_Controller_Router_Rewrite */
        $router->useRequestParametersAsGlobal(true);
        if($router instanceof Zend_Controller_Router_Rewrite){
            $path = APPLICATION_PATH . "/configs/routes.xml";
            $config = new Zend_Config_Xml($path, 'routes');
            $router->setChainNameSeparator('.')->addConfig($config);
        }
        $this->getFrontController()->setRouter($router);
        return $router;
    }

    public  function _initAuth(){
        $service = new CMS_Service_Auth();
        $service->setSalt('SYukaL~6gDpsBgeeo9P8XdFRoa0%B^GJh|h%lBCVP blI256vtw~KJ0G2U~k');
        return $service;
    }

    public function _initView(){
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        /* @var $viewRenderer Zend_Controller_Action_Helper_ViewRenderer */
        $view = new CMS_View($this->getOptions()->resources->view->toArray());
        $viewRenderer->setView($view);
        return $view;
    }

    public function _initMail(){
        $opt = $this->getOptions()->resources->mail;

        $config = array(
            'auth'          => $opt->transport->auth,
            'username'      => $opt->transport->username,
            'password'      => $opt->transport->password,
            'ssl'           => $opt->transport->ssl,
            'port'          => $opt->transport->port
        );

        if(!$config['ssl'] || empty($config['ssl'])){
            unset($config['ssl']);
        }

        $transport = new Zend_Mail_Transport_Smtp($opt->transport->host, $config);

        $mail = new CMS_Service_Mail($this->view);
        $mail->setDefaultFrom($opt->defaultFrom->email, $opt->defaultFrom->name);
        $mail->setDefaultReplyTo($opt->defaultReplyTo->email, $opt->defaultReplyTo->name);
        $mail->setDefaultTransport($transport);
        return $mail;
    }

    public function _initModel(){
        $this->bootstrap('db');
        $mailer = new Service_MailSender($this->mail);
        $mailer->setView($this->view);
        $model = new Service_ModelRepository($this->db);
        $model->setAuth($this->auth);
        $model->setMailer($mailer);
        $this->view->setModel($model);
        return $model;
    }

    public function _initAcl(){
        $this->bootstrap('model');
        $acl = $this->model->getAcl();
        if($this->auth->hasIdentity()) $role = $this->model->user()->getRole();
        else $role = new Zend_Acl_Role(Model_Acl::ROLE_GUEST);

        $acl->setDefaultRole($role);
        $plugin = new CMS_Acl_Plugin($acl);
        if($this->auth){
            $plugin->setAuth($this->auth);
        }
        $this->getFrontController()->registerPlugin($plugin);
        $this->view->setAcl($acl);
        Zend_Registry::set('CMS_Acl', $acl);
        return $acl;
    }

    public function _initNavigation(){
        $service = new CMS_Service_Navigation(new Zend_Navigation(new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml')));
        if($this->view) $service->setViewHelper($this->view->navigation());
        $this->bootstrap('acl');
        $service->setAcl($this->acl, $this->acl->getDefaultRole());
        if($this->auth->hasIdentity()) $service->setAclRole($this->model->user()->getRole());
        $ser2 = new CMS_Navigation(new Zend_Config_Xml(APPLICATION_PATH . '/configs/nav.xml'));
        $this->view->setNavigation($ser2);
        Zend_Registry::set('CMS_Navigation', $ser2);
        return $service;
    }

    public function _initPaginator(){
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('partial/pagination.phtml');
        CMS_Form_Horizontal::setModel($this->model);
        CMS_Form_Inline::setModel($this->model);
    }


}