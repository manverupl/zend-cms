<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-25 23:08
 */

class Plugin_LayoutSwitcher extends Zend_Controller_Plugin_Abstract
{
    public function  preDispatch(Zend_Controller_Request_Abstract $request){
        if($request instanceof Zend_Controller_Request_Http){
            if($request->getControllerName() == 'error'){
                Zend_Layout::getMvcInstance()->setLayout('error');
            }
            else {
                if ($request->getModuleName() == 'admin' || $request->getModuleName() == 'panel') {
                    Zend_Layout::getMvcInstance()->setLayout('panel');
                }
            }
        }
    }
}