<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 00:25
 */

class Service_ModelRepository extends CMS_Model_Repository
{
    /**
     * @var CMS_Service_Auth
     */
    protected $auth;
    /**
     * @var Model_Acl
     */
    protected $acl;
    /**
     * @var Service_MailSender
     */
    protected $mailer;

    public function setAuth(CMS_Service_Auth $auth)
    {
        $this->auth = $auth;
        return $this;
    }

    public function getAuth()
    {
        if(!$this->auth)
            throw new CMS_Model_Exception('Auth service is not available.');
        return $this->auth;
    }

    public function getAcl()
    {
        if(!$this->acl)
            $this->acl = new Model_Acl($this);
        return $this->acl;
    }

    public function setMailer(Service_MailSender $mailer){
        $this->mailer = $mailer;
        return $this;
    }

    public function getMailer(){
        if($this->mailer) return $this->mailer;
        else throw new CMS_Model_Exception('Mailer service is not set.');
    }

    public function users(){
        return new Model_DbTable_User(array(Zend_Db_Table::ADAPTER => $this->adapter));
    }

    public function user(){
        if($this->auth->hasIdentity()){
            $user = $this->users()->findById($this->auth->getIdentity()->id);
            if(!$user) {
                $this->auth->clearIdentity();
                throw new CMS_Model_Exception('Identity exists but no corresponding account was found');
            }
            return $user;
        }
        throw new CMS_Model_Exception('Identity not found.');
    }

    public function sites(){
        return new Model_DbTable_Site(array(Zend_Db_Table::ADAPTER => $this->adapter));
    }

    public function news(){
        return new Model_DbTable_News(array(Zend_Db_Table::ADAPTER => $this->adapter));
    }
}