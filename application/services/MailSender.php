<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-31 17:43
 */

class Service_MailSender
{
    /**
     * @var CMS_Service_Mail
     */
    protected $mta;
    /**
     * @var CMS_View
     */
    protected $view;
    protected $subjectPrefix;

    public function __construct(CMS_Service_Mail $mta, $subjectPrefix = ''){
        $this->subjectPrefix = $subjectPrefix;
        $this->mta = $mta;
    }

    public function setView($view){
        $this->view = $view;
        return $this;
    }

    /**
     * @return CMS_View
     */
    public function getView(){
        return $this->view;
    }

    /**
     * @return CMS_Service_Mail
     */
    public function createEmail(){
        return clone $this->mta;
    }

    /**
     * @param Model_User $recipient
     * @param $subject
     * @param $viewScript
     * @param array $params
     * @return Service_MailSender
     * @throws Zend_Mail_Exception
     */
    public function sendEmail(Model_User $recipient, $subject, $viewScript, array $params = null)
    {
        $mail = $this->createEmail()->setViewScript($viewScript)
                                    ->assign($params)
                                    ->addTo($recipient->getEmail())
                                    ->setSubject($this->subjectPrefix . $subject);

        return $this->send($mail);

    }

    public function sendContactEmail($data, $recipient, $subject){
        $mail = $this->createEmail()->setViewScript('email/landing/contact.phtml')
            ->assign($data)
            ->addTo($recipient)
            ->setSubject($this->subjectPrefix . $subject);
        return $this->send($mail);
    }

    /**
     * @param Zend_Mail $mail
     * @return Service_MailSender|null
     */
    public function send(Zend_Mail $mail){
        try {
            $mail->send();
        } catch(Exception $e){
            return null;
        }
        return $this;
    }
}