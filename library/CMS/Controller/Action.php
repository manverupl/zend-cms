<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-25 22:37
 *
 * @method Zend_Controller_Response_Http getResponse()
 * @method Zend_Controller_Request_Abstract|Zend_Controller_Request_Http getRequest()
 */

class CMS_Controller_Action extends Zend_Controller_Action
{

    /**
     * @return Zend_Controller_Front
     */
    public function getFrontController(){
        return Zend_Controller_Front::getInstance();
    }

    /**
     * @return mixed
     */
    public function getAuth(){
        return $this->getResource('auth');
    }

    /**
     * @return CMS_Model_Repository
     */
    public function getModel(){
        return $this->getResource('model');
    }

    /**
     * @param Model_User $user
     * @param $password
     * @return mixed
     */
    public function authenticate(Model_User $user, $password)
    {
        return $this->getAuth()->authenticate($user->getEmail(), $password);
    }
    /**
     * Get bootstrap
     *
     * @return Bootstrap
     */
    protected function getBootstrap()
    {
        return $this->getInvokeArg('bootstrap');
    }

    /**
     * Get bootstrap resource (use serviceManager instead)
     *
     *
     * @param string $name
     * @return mixed bootstrap resource
     */
    protected function getResource($name)
    {
        if (!$this->getBootstrap()->hasResource($name)) {
            $this->getBootstrap()->bootstrap($name);
        }

        return $this->getBootstrap()->getResource($name);
    }

    /**
     * @param Zend_Validate_Interface $validator
     * @param null $data
     * @return bool
     */
    protected function isValidPost(Zend_Validate_Interface $validator, $data = null)
    {
        if (!$this->getRequest()->isPost()) {
            return false;
        }
        if ($data === null) {
            $data = $this->getRequest()->getPost();
        }
        if ($validator->isValid($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param null $route
     * @param array $options
     * @param bool $reset
     */
    protected function gotoRoute($route = null, array $options = array(), $reset = false)
    {
        if (empty($options)) {
            $options = $this->_getAllParams();
        }
        $this->_helper->redirector->gotoRouteAndExit($options, $route, $reset);
    }

    /**
     * @param null $route
     * @param array $options
     * @param bool $reset
     */
    protected function gotoRouteAndExit($route = null, array $options = array(), $reset = false)
    {
        $this->gotoRoute($route, $options, $reset);
    }

    public function flashMessage($message, $params = array())
    {
        /* @var $fm Zend_Controller_Action_Helper_FlashMessenger */
        $fm = $this->getHelper('flashMessenger');
        if ($message) {
            $fm->addMessage(vsprintf($message, $params));
        }

        return $fm;
    }

    public function getPaginator($items, $page = null, $itemsPerPage = null)
    {
        $paginator = Zend_Paginator::factory($items);

        if (!is_null($itemsPerPage)) {
            $paginator->setItemCountPerPage($itemsPerPage);
        }
        if (!is_null($page)) {
            $paginator->setCurrentPageNumber($page);
        } elseif ($this->getRequest()->has('page')) {
            $paginator->setCurrentPageNumber($this->getRequest()->getParam('page'));
        }

        return $paginator;
    }


}