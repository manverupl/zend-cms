<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-25 22:38
 */

class CMS_Controller_Error extends CMS_Controller_Action
{
    /**
     * @return null|Zend_Exception
     */
    public function getException(){
        return (($e = $this->getError()) !== null && isset($e['exception'])) ? $e['exception'] : null;
    }

    /**
     * @return array
     */
    public function getError(){
        return $this->getParam('error_handler', null);
    }

    public function canDisplayException(){
        $fc = $this->getFrontController();
        return $fc->getParam('bootstrap')->getEnvironment() != 'production' && $fc->getParam('displayExceptions');
    }
}