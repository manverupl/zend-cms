<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 16:14
 */

class CMS_Form_Horizontal extends CMS_Form_Abstract
{
    public function __construct($options = null){
        $this->setClass('form-horizontal');
        parent::__construct($options);
        $this->setElementDecorators(
            array(
                array('Composite'),
            )
        );
    }
}