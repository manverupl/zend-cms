<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 15:58
 */

class CMS_Form_Abstract extends Zend_Form
{
    const SUBMIT_BUTTON_NAME = 'formSubmit';
    const SEARCH_CONSTRAINT_VALUE_IF_NULL = 'valueIfNull';
    const SEARCH_CONSTRAINT_VALUE_IF_EMPTY = 'valueIfEmpty';

    protected static $model;

    public function __construct($options = null){
        $this
            ->addElementPrefixPath('Filter', APPLICATION_PATH . '/filters', 'filter')
            ->addElementPrefixPath('Validator', APPLICATION_PATH . '/validators', 'validate')
            ->addElementPrefixPath('CMS_Validator', 'CMS/Validator/', 'validate')
            ->addElementPrefixPath('CMS_Filter', 'CMS/Filter/', 'filter')

            ->addPrefixPath('CMS_Form_Element', 'CMS/Form/Element', self::ELEMENT)
            ->addPrefixPath('CMS_Form_Decorator', 'CMS/Form/Decorator', self::DECORATOR)
            ->addPrefixPath('Form_Decorator', APPLICATION_PATH . '/forms/decorators', self::DECORATOR);

        $this->setDecorators(
            array(
                'FormElements',
                'Form'
            )
        );
        $className = get_class($this);

        $f1 = new Zend_Filter_Word_CamelCaseToUnderscore();
        $formName = $className = $f1->filter($className);
        $f2 = new Zend_Filter_Word_UnderscoreToDash();
        $className = strtolower($f2->filter($className));

        $this->addClass($className);

        if (!$this->getName()) {
            $f3 = new Zend_Filter_Word_UnderscoreToCamelCase();
            $formName = $f3->filter($formName);
            $this->setName(lcfirst($formName));
        }

        parent::__construct($options);
    }
    public static function setModel(CMS_Model_Repository $repository){
        self::$model = $repository;
    }
    public function addClass($class)
    {
        if (!isset($this->_attribs['class'])) {
            $this->_attribs['class'] = array();
        }
        $current = $this->_attribs['class'];
        if (is_string($current)) {
            if (!empty($current)) {
                $current = array($current => $current);
            } else {
                $current = array();
            }
        }
        if (is_string($class)) {
            $class = explode(' ', $class);
        }
        foreach ($class as $id => $name) {
            $name = trim($name);
            $current[$name] = $name;
        }
        $this->_attribs['class'] = $current;
        return $this;
    }
    public function setClass($class)
    {
        $this->_attribs['class'] = array();
        $this->addClass($class);

        return $this;
    }
    public function removeClass($class)
    {
        if (isset($this->_attribs['class'][$class])) {
            unset($this->_attribs['class'][$class]);
        }

        return $this;
    }
    public function clearClass()
    {
        $this->_attribs['class'] = array();

        return $this;
    }
    public function createSubmitButton($label = 'Wyślij', $name = self::SUBMIT_BUTTON_NAME)
    {
        return $this->createElement(
            'button',
            $name,
            array(
                'type' => 'submit',
                //'value'     => $label,
                //'content'   => $label,
                'label' => $label,
                'ignore' => true,
                'class' => 'btn',
            )
        );
    }
    public function addSubmitButton($label = 'Wyślij', $name = self::SUBMIT_BUTTON_NAME)
    {
        $button = $this->createSubmitButton($label, $name);
        $button->setIgnore(true);
        $this->addElement($button);

        return $this;
    }
    public function renderOpeningTag()
    {
        $form = new Zend_View_Helper_Form();
        $form->setView($this->getView());

        return $form->form($this->getName(), $this->getAttribs());
    }
    public function renderErrors($title = null, array $options = array())
    {
        if (!$this->hasErrors()) {
            return;
        }
        $defaults = array(
            'markupListStart' => '<div class="alert alert-error"><ul>',
            'markupListEnd' => '    </ul>' . PHP_EOL . '   </div>',
            'markupElementLabelStart' => '<strong>',
            'markupElementLabelEnd' => '</strong>',
        );
        if ($title !== null) {
            $defaults['markupListStart'] =
                '<div class="alert alert-error">' . PHP_EOL
                . '   <h3>' . $title . '</h3>' . PHP_EOL
                . '   <ul>';
        }

        $options = $options + $defaults;
        $d = new Zend_Form_Decorator_FormErrors($options);
        $d->setElement($this);

        return $d->render(null);
    }
    public function getModel()
    {
        if (!self::$model) {
            throw new CMS_Exception('Model was not set');
        }

        return self::$model;
    }
    public function render(Zend_View_Interface $view = null)
    {
        foreach ($this->getElements() as $element) {
            if ($element instanceof Zend_Form_Element_File) {
                $element->setDecorators(array('File', 'Composite'));
            }
        }
        return parent::render($view);
    }
    public function getValuesAsFlatArray()
    {
        $tmp = array();
        $a = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->getValues(false)));
        foreach ($a as $key => $value) {
            $tmp[$key] = $value;
        }

        return $tmp;
    }
    public function removeSubmitButton($name = self::SUBMIT_BUTTON_NAME)
    {
        $this->removeElement($name);

        return $this;
    }
    public function removeElements(array $elements)
    {
        foreach ($elements as $name) {
            $this->removeElement($name);
        }

        return $this;
    }
    public function getSubmitButton($name = self::SUBMIT_BUTTON_NAME)
    {
        return $this->getElement($name);
    }
    public function setElementsAttribute($name, $value, array $exclude = array())
    {
        foreach ($this->getElements() as $element) {
            if (in_array($element->getName(), $exclude)) {
                continue;
            }
            $element->setAttrib($name, $value);
        }

        return $this;
    }
    public function processSearch(array $data)
    {
        $defaults = $this->getValues();
        $data = $this->getValidValues($data);
        $notEmptyValidator = new Zend_Validate_NotEmpty();

        $clean = array();
        foreach ($defaults as $name => $value) {

            $attribs = $this->getElement($name)->getAttribs();

            if (!isset($data[$name]) || ($data[$name] === null)) {
                if (isset($attribs[self::SEARCH_CONSTRAINT_VALUE_IF_NULL])) {
                    $clean[$name] = $attribs[self::SEARCH_CONSTRAINT_VALUE_IF_NULL];
                    continue;
                }
            }

            $isEmpty = !isset($data[$name]) || !$notEmptyValidator->isValid($data[$name]);
            if ($isEmpty) {
                if (isset($attribs[self::SEARCH_CONSTRAINT_VALUE_IF_EMPTY])) {
                    $clean[$name] = $attribs[self::SEARCH_CONSTRAINT_VALUE_IF_EMPTY];
                    continue;
                }
            }

            if (!$isEmpty) {
                $clean[$name] = $data[$name];
            } elseif ($value !== null) {
                $clean[$name] = $value;
            }
        }

        return $clean;
    }
    public function disable()
    {
        $this->setElementsAttribute('disabled', 'disabled');

        return $this;
    }
}