<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 16:16
 */
class CMS_Form_Inline extends CMS_Form_Abstract
{
    public function __construct($options = null){
        $this->setClass('form-inline');
        parent::__construct($options);
        $this->setElementDecorators(
            array(
                array('Composite', array('renderInputsOnly' => 1)),
            )
        );
    }
}