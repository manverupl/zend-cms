<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-28 11:00
 */

class CMS_Form_Element_Email extends Zend_Form_Element_Text
{
    public $helper = 'formEmail';
}