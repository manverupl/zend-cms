<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 16:14
 */

class CMS_Form_Decorator_Composite extends Zend_Form_Decorator_Abstract
{

    /**
     * Main wrapper class. Unused if renderInputOnly is set to true.
     *
     * @var string
     */
    protected $mainWrapperClass = 'form-group';

    public function __construct($options = null)
    {
        parent::__construct($options);
        if ($this->getOption('mainWrapperClass')) {
            $this->mainWrapperClass = $this->getOption('mainWrapperClass');
        }
    }

    /**
     * @param null   $content
     * @param bool   $prepend
     * @param string $class
     * @return mixed
     */
    public function buildLabel($content = null, $prepend = true, $class = 'col-xs-2 control-label')
    {
        $element = $this->getElement();

        $label = $element->getLabel();
        if (empty($label)) {
            return '';
        }

        if ($translator = $element->getTranslator()) {
            $label = $translator->translate($label);
        }

        if (!empty($content)) {
            if ($prepend) {
                $label = $content . $label;
            } else {
                $label = $label . $content;
            }
        }

        return $element->getView()
            ->formLabel($element->getName(), $label, array('escape' => false, 'class' => $class));
    }

    /**
     * @return string
     */
    public function buildInput()
    {
        $element = $this->getElement();
        $helper = $element->helper;

        if ($element->helper == 'formSubmit' || $element->helper == 'formButton') {
            $class = $element->getAttrib('class');
            $element->setAttrib('class', 'btn ' . $class);
        }
        elseif($element->helper == 'formCheckbox'){
            $class = $element->getAttrib('class');
        }
        else{
            $class = $element->getAttrib('class');
            $element->setAttrib('class', $class . ' form-control');
        }


        $attribs = $element->getAttribs();


        $append = $prepend = '';
        $wrap = false;
        $wrapClass = array();

        if (isset($attribs['prepend'])) {
            $wrap = true;
            $wrapClass[] = 'input-prepend';
            $prepend = '<span class="add-on">' . $attribs['prepend'] . '</span>';
        }

        if (isset($attribs['icon'])) {
            $wrap = true;
            if ($element instanceof Zend_Form_Element_Button) {
                $element->setLabel('<i class="icon-' . $attribs['icon'] . '"></i> ' . $element->getLabel());
                $element->setAttrib('escape', false);
            } else {
                $wrapClass[] = 'input-prepend';
                $prepend = '<span class="add-on"><i class="icon-' . $attribs['icon'] . '"></i></span>';
            }
        }

        if (isset($attribs['append'])) {
            $wrap = true;
            $wrapClass[] = 'input-append';
            $append = '<span class="add-on">' . $attribs['append'] . '</span>';
        }

        if (isset($attribs['icon-append'])) {
            $wrap = true;
            if ($element instanceof Zend_Form_Element_Button) {
                $element->setLabel($element->getLabel() . ' <i class="icon-' . $attribs['icon-append'] . '"></i>');
                $element->setAttrib('escape', false);
            } else {
                $wrapClass[] = 'input-prepend';
                $append = '<span class="add-on"><i class="icon-' . $attribs['icon-append'] . '"></i></span>';
            }
        }

        if (isset($attribs['inline-help'])) {
            unset($attribs['inline-help']);
        }
        if (isset($attribs['prepend'])) {
            unset($attribs['prepend']);
        }
        if (isset($attribs['append'])) {
            unset($attribs['append']);
        }
        if(isset($attribs['jQueryParams'])){
             $attribs = $attribs['jQueryParams'];
            unset($attribs['jQueryParams']);

        }

        foreach ($attribs as $aname => $avalue) {
            $element->setAttrib($aname, $avalue);
        }


        $viewHelper = new Zend_Form_Decorator_ViewHelper();

//        $viewHelper->getElement($element)->setHelper($element->helper);
        $viewHelper->setElement($element)
            ->setHelper($element->helper);
        $element = $viewHelper->render(null);



        if ($wrap) {
            $element = '<div class="' . implode(' ', $wrapClass) . '">' . $prepend . $element . $append . '</div>';
        }


        return $element;
    }

    /**
     * @return string
     */
    public function buildErrors()
    {
        if (!$this->getElement()->hasErrors()) {
            return '';
        }

        $element = $this->getElement();
        $messages = $element->getMessages();

        if (empty($messages)) {
            return '';
        }

        return '<span class="help-block">' .
            $element->getView()->formErrors($messages) . '</span>';
    }

    /**
     * @return string
     */
    public function buildDescription()
    {
        $element = $this->getElement();
        $desc = $element->getDescription();
        if (empty($desc)) {
            return '';
        }

        return '<div class="help-block">' . $desc . '</div>';
    }

    /**
     * @return string
     */
    public function buildInlineHelp()
    {
        $element = $this->getElement();
        $inline = $element->getAttrib('inline-help');

        if (!empty($inline)) {
            return '<span class="help-inline">' . $inline . '</span>';
        }
    }

    /**
     * @param string $content
     * @return string
     */
    public function render($content)
    {
        return $this->renderInput($content);
    }

    /**
     * Render input according to type
     *
     * @param $content
     * @return mixed|string
     */
    protected function renderInput($content)
    {
        $element = $this->getElement();
        $class = array();
        $class[] = 'element-' . $element->getName();
        if (!$element instanceof Zend_Form_Element) {
            return $content;
        }
        if (null === $element->getView()) {
            return $content;
        }
        $messages = $element->getMessages();

        $class[] = $this->mainWrapperClass;
        if ($element->hasErrors()) {
            $class[] = 'has-error';
        }
        if ($element->isRequired()) {
            $class[] = 'required';
            $element->setAttrib('required', 'required');
        }

        $label = $desc = $errors = '';

        switch ($element->helper) {
            case 'formCheckbox':
                $label = $this->buildLabel();
                $input = $this->buildInput();
                $desc = $this->buildDescription();
                $errors = $this->buildErrors();

                break;
            case 'formMultiCheckbox':
            case 'formRadio':
                $class[] = 'multi';
                $label = $this->buildLabel();
                $input = $this->buildInput();
                $errors = $this->buildErrors();
                $desc = $this->buildDescription();
                break;
            case 'formSubmit':
            case 'formButton':
                $label = '';
                $input = $this->buildInput();
                $errors = $this->buildErrors();
                $desc = $this->buildDescription();

                break;
            case 'formHidden':
                $input = $this->buildInput();
                break;
            default:
                $label = $this->buildLabel();
                $input = $this->buildInput();
                $errors = $this->buildErrors();
                $desc = $this->buildDescription();

        }

        $inlineHelp = $this->buildInlineHelp();

        if ($element->helper == 'formHidden') {
            return $input;
        }

        if ((bool)$this->getOption('renderSimple')) {
            return $label . $input . $inlineHelp . $desc . $errors;
        }

        if ((bool)$this->getOption('renderInputsOnly')) {
            return $input . $inlineHelp . $desc . $errors;
        } else {
            return
                '<div class="' . implode(' ', $class) . '">' . $label
                . '<div class="col-xs-10">' . $input . $inlineHelp . $desc . $errors . '</div></div>';
        }
    }
}