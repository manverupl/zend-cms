<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-04-08 10:58
 */

class CMS_View_Helper_Page extends CMS_View_Helper_Abstract
{
    /**
     * @var CMS_Navigation
     */
    protected $_nav;
    /**
     * @var CMS_Navigation_Page
     */
    protected $_page;
    /**
     * @var string
     */
    protected $_titleSeparator = "::";
    public function page(){
        if(!$this->_nav){
            if(Zend_Registry::isRegistered('CMS_Navigation')) $this->_nav = Zend_Registry::get('CMS_Navigation');
        }
        if(!$this->_nav) throw new CMS_View_Exception('Navigation container is not set');
        $this->_page = $this->_nav->findDeepestActivePage();
        return $this;
    }
    public function setNav($nav){
        $this->_nav = $nav;
        return $this;
    }

    public function setTitleSeparator($titleSeparator){
        $this->_titleSeparator = $titleSeparator;
        return $this;
    }

    public function getRequest(){
        return Zend_Controller_Front::getInstance()->getRequest();
    }

    public function headStyle(){
        return $this->getView()->getHelper('HeadStyle');
    }

    public function headTitle(){
        return $this->getView()->getHelper('HeadTitle');
    }

    public function headScript(){
        return $this->getView()->getHelper('HeadScript');
    }

    public function headLink(){
        return $this->getView()->getHelper('HeadLink');
    }

    public function headMeta(){
        return $this->getView()->getHelper('HeadMeta');
    }

    public function title(){
        $args = func_get_args();
        $pages = array();
        $tm = $this->_page->getTitle();
        if(strpos('%s', $tm) !== null){
            $tm = vsprintf($tm, $args);
        }
        array_push($pages, $tm);
        $par = $this->_page->getParent();
        while($par instanceof CMS_Navigation_Page){
            array_push($pages, $par->getTitle());
            $par = $par->getParent();
        }
        $pages = array_reverse($pages, false);
        $title = join($this->_titleSeparator, $pages);
        return $title;
    }

    public function __get($property){
        return $this->_page->$property;
    }

}