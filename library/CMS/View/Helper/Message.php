<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 14:29
 */

class CMS_View_Helper_Message extends CMS_View_Helper_Abstract
{
    /**
     * @param null $message
     * @param null $cssClass
     * @return string
     */
    public function message($message = null, $cssClass = null)
    {
        if (empty($message)) {
            return '';
        }
        if (empty($cssClass)) {
            $cssClass = 'alert-info';
        }
        return
            '<div class="alert alert-dismissible ' . $cssClass . '">
		<button class="close" data-dismiss="alert">&times;</button>
		' . $message . '</div>';
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->message();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}