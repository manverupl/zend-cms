<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-04-08 11:36
 */

class CMS_View_Helper_Nav_Breadcrumbs extends CMS_View_Helper_Abstract
{
    /**
     * @var CMS_Navigation
     */
    protected $_nav;
    /**
     * @var CMS_Navigation_Page
     */
    protected $_page;

    public function breadcrumbs(){
        if(!$this->_nav){
            if(Zend_Registry::isRegistered('CMS_Navigation')) $this->_nav = Zend_Registry::get('CMS_Navigation');
        }
        if(!$this->_nav) throw new CMS_View_Exception('Navigation container is not set');
        $this->_page = $this->_nav->findDeepestActivePage();
        return $this;
    }

    public function setNav($nav){
        $this->_nav = $nav;
        return $this;
    }

    public function renderBreadcrubs(){
        $pages = array();
        array_push($pages, $this->_page);
        $par = $this->_page->getParent();
        while($par instanceof CMS_Navigation_Page){
            array_push($pages, $par);
            $par = $par->getParent();
        }
        $pages = array_reverse($pages, false);
        $breadcrumbs = '<ol class="breadcrumb">';
        $count = count($pages);
        foreach($pages as $page){
            if($page->getTitle() != '') {
                if ($count > 1) {
                    $breadcrumbs .= '<li><a href="' . $page->getHref() . '">' . $page->getTitle() . '</a>';
                } else {
                    $breadcrumbs .= '<li class="active">' . $page->getTitle() . '</li>';
                }
            }
            $count--;
        }
        $breadcrumbs .= '</ol>';
        return $breadcrumbs;
    }

    public function __toString(){
        return $this->renderBreadcrubs();
    }
}