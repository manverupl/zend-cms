<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 14:40
 */

class CMS_View_Helper_Blocks extends CMS_View_Helper_Abstract
{
    protected $elements = array();

    /**
     * @var array of keywords
     */
    protected $placement = array();

    public function __construct()
    {
    }

    /**
     * @return CMS_View_Helper_Blocks
     */
    public function blocks()
    {
        return $this;
    }

    /**
     * Return block count for specified place
     *
     * @param string $placement
     * @return int
     */
    public function count($placement = 'default')
    {
        if (isset($this->placement[$placement])) {
            return count($this->placement[$placement]);
        }

        return 0;
    }

    /**
     * Add new aside element
     *
     * @param string $path
     * @param string $placement tag OPTIONAL
     * @param bool   $prepend   prepend other blocks marked with this tag ?
     * @return CMS_View_Helper_Blocks
     */
    public function addElement($path, $placement = 'default', $prepend = false)
    {
        $id = count($this->elements);
        $this->elements[$id] = $path;

        if (!isset($this->placement[$placement])) {
            $this->placement[$placement] = array();
        }

        if ($prepend) {
            array_unshift($this->placement[$placement], $id);
        } else {
            $this->placement[$placement][] = $id;
        }

        return $this;
    }

    /**
     * @param string $placement
     * @return string
     */
    public function render($placement = 'default')
    {
        $result = '';
        if (isset($this->placement[$placement])) {
            foreach ($this->placement[$placement] as $block => $id) {
                $element = $this->elements[$id];
                $element = preg_replace('#(.*)\.phtml$#', '\\1', $element);

                $result .= $this->getView()->render($element . '.phtml');
            }
        }

        return $result;
    }

    /**
     * @see render()
     */
    public function __toString()
    {
        try{
            return $this->render();
        } catch(Exception $e){
            return 'Could not render aside block: ' . $e->getMessage() . '';
        }
    }
}