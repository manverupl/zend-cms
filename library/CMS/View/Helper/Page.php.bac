<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 14:37
 */

class CMS_View_Helper_Page extends CMS_View_Helper_Abstract
{
    /**
     * @var CMS_View_Helper_Blocks
     */
    protected $blocks;

    /**
     * @var array
     */
    protected $properties = array(
        'title' => 'Untitled page',
        'translate' => array(),
        'layout' => 'default',
        'description' => '',
        'keywords' => array(),
        'blocks' => array(),
    );

    public function __construct()
    {
        $this->layout = $this->getLayout()->getLayout();

        if (!$this->getView()) {
            $this->setView($this->getLayout()->getView());
        }

        $naviPage = $this->findDeepestActivePage();
        if ($naviPage) {
            if (isset($naviPage->title) && !empty($naviPage->title)) {
                $this->title = $naviPage->title;
            } else {
                $this->title = $naviPage->label;
            }

            $this->description = $naviPage->description;
            $this->keywords = $naviPage->keywords;

            if ($naviPage->layout) {
                $this->layout = $naviPage->layout;
            }

            $this->headTitle()->headTitle($naviPage->label);

            if ($naviPage->blocks) {

                $blocks = array();
                if (isset($naviPage->blocks['block']['src'])) {
                    $blocks = array($naviPage->blocks['block']);
                } else {
                    $blocks = $naviPage->blocks['block'];
                }

                foreach ($blocks as $block) {
                    if (!isset($block['src'])) {
                        continue;
                    }
                    !isset($block['placement']) ? $block['placement'] = null : null;
                    $this->blocks()->addElement($block['src'], $block['placement']);
                }
            }
        }
    }

    /**
     * Add new set of properties (title, layout, description, properties)
     *
     * @param array|string $properties
     * @return CMS_View_Helper_Page
     */
    public function page($properties = array())
    {
        if (isset($properties['blocks'])) {
            if (!is_array($properties['blocks'])) {
                $properties['blocks'] = array($properties['blocks']);
            }
            foreach ($properties['blocks'] as $id => $block) {
                $this->blocks()->addElement($block);
            }
        }
        foreach ($properties as $property => $value) {
            $this->setProperty($property, $value);
        }

        return $this;
    }

    /**
     * Set page property
     *
     * @param string $property
     * @param string $value
     * @return CMS_View_Helper_Page
     */
    public function setProperty($property, $value)
    {
        $property = strtolower($property);
        switch ($property) {
            case 'translate':
                $this->setTranslateParams($value);
                break;
            case 'title':
                $this->properties[$property] = $value;
                break;
            case 'layout':
                if ($this->getLayout()->isEnabled()) {
                    $this->getLayout()->setLayout($value);
                    $this->properties[$property] = $value;
                } else {
                    $this->properties[$property] = null;
                }
                break;
            case 'keyword':
            case 'keywords':
                $this->addKeyword($value);
                break;
            default:
                $this->properties[$property] = $value;
        }

        return $this;
    }

    /**
     * Set title translation params
     *
     * @deprecated
     * @param mixed $params
     * @return CMS_View_Helper_Page
     */
    public function setTranslateParams($params)
    {
        if (!is_array($params)) {
            $params = array($params);
        }

        foreach ($params as $param) {
            $this->properties['translate'] = $param;
        }

        return $this;
    }

    /**
     * Add keywords
     *
     * @param string|array $keywords
     * @return CMS_View_Helper_Page
     */
    public function addKeyword($keywords)
    {
        if (!is_array($keywords)) {
            $keywords = array($keywords);
        }

        foreach ($keywords as $keyword) {
            $this->properties['keywords'][] = $keyword;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getKeywordsAsString()
    {
        return implode(', ', $this->keywords);
    }

    /**
     * @return Zend_Controller_Request_Http
     */
    public function getRequest()
    {
        return Zend_Controller_Front::getInstance()->getRequest();
    }

    /**
     * @return Zend_View_Helper_HeadStyle
     */
    public function headStyle()
    {
        return $this->getView()->getHelper('HeadStyle');
    }

    /**
     * @return Zend_View_Helper_HeadTitle
     */
    public function headTitle()
    {
        return $this->getView()->getHelper('HeadTitle');
    }

    /**
     * @return Zend_View_Helper_HeadScript
     */
    public function headScript()
    {
        return $this->getView()->getHelper('HeadScript');
    }

    /**
     * @return Zend_View_Helper_HeadLink
     */
    public function headLink()
    {
        return $this->getView()->getHelper('HeadLink');
    }

    /**
     * @return Zend_View_Helper_HeadMeta
     */
    public function headMeta()
    {
        return $this->getView()->getHelper('HeadMeta');
    }

    /**
     * Get translated title
     *
     * @param array|string $params Translation params
     * @return string
     */
    public function title($params = array())
    {
        if (!is_array($params)) {
            $params = array($params);
        }
        $title = $this->getView()->translate($this->properties['title'], $params);

        return $title;
    }

    /**
     * @return CMS_View_Helper_Blocks
     */
    public function blocks()
    {
        if (!$this->blocks) {
            $this->blocks = $this->getView()->getHelper('Blocks');
        }

        return $this->blocks;
    }

    /**
     * @return Zend_Navigation_Page|null
     */
    protected function findDeepestActivePage()
    {
        $naviHelper = $this->getView()->navigation();
        $renderInvisible = $naviHelper->getRenderInvisible();
        $result = $naviHelper->setRenderInvisible(true)->findActive($naviHelper->getContainer());
//        print_r($result);exit;
        // renderInvisible so all menu items will be searched for, then revert the setting
        $naviHelper->setRenderInvisible($renderInvisible);
        if (empty($result)) {
            return null;
        }

        return $result['page'];
    }

    /**
     * @param $property
     * @return bool
     */
    public function __isset($property)
    {
        if (isset($this->properties[$property])) {
            return true;
        }

        return false;
    }

    /**
     * Magic set property
     *
     * @param string $property
     * @param string $value
     * @return CMS_View_Helper_Page
     */
    public function __set($property, $value)
    {
        return $this->setProperty($property, $value);
    }

    /**
     * Magic get property
     *
     * @param string $property
     * @return View_Helper_Page
     */
    public function __get($property)
    {
        if (isset($this->properties[$property])) {

            if ($property == 'title' && !empty($this->properties['translate'])) {
                $title = $this->getView()->translate(
                    $this->properties['title'],
                    $this->properties['translate']
                );

                return $title;
            }

            return $this->properties[$property];
        }

        return null;
    }
}