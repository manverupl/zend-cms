<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 00:11
 *
 */

class CMS_View_Helper_Route extends Zend_View_Helper_Url
{
    public function route($routeName = null, array $options = null, $reset = false, $encode = true)
    {
        if (is_null($options)) {
            $options = array();
        }
        $requestParams = Zend_Controller_Front::getInstance()->getRequest()->getParams();
        $options = array_merge($requestParams, $options);

        return parent::url($options, $routeName, $reset, $encode);
    }
}