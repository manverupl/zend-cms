<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-04-02 17:46
 */

class CMS_View_Helper_Nav extends CMS_View_Helper_Abstract
{
    /**
     * @var Model_Acl
     */
    protected $_acl;
    protected $_useAcl = true;
    protected $_ulClass;
    protected $_nestedUlClass;
    protected $_activeClass = 'active';
    /**
     * @var CMS_Navigation
     */
    protected $_container;
    protected $_maxDepth = 0;
    protected $_titledMenu = false;
    protected $_titleClass = '';
    protected $_titleTag = 'div';


    public function nav(CMS_Navigation_Container $container = null){
        if($container) $this->_container = $container;
        elseif(Zend_Registry::isRegistered('CMS_Navigation')) $this->_container = Zend_Registry::get('CMS_Navigation');
        if(Zend_Registry::isRegistered('CMS_Acl')) $this->setAcl(Zend_Registry::get('CMS_Acl'));
        return $this;
    }

    public function setAcl(CMS_Acl $acl){
        $this->_acl = $acl;
        return $this;
    }

    public function getAcl(){
        return $this->_acl;
    }

    public function setUseAcl($useAcl){
        $this->_useAcl = $useAcl;
        return $this;
    }
    public function getUseAcl(){
        return $this->_useAcl;
    }

    public function setUlClass($ulClass){
        $this->_ulClass = $ulClass;
        return $this;
    }
    public function setNestedUlClass($nestedUlClass){
        $this->_nestedUlClass = $nestedUlClass;
        return $this;
    }
    public function setActiveClass($activeClass){
        $this->_activeClass = $activeClass;
        return $this;
    }
    public function setContainer(CMS_Navigation_Container $container){
        $this->_container = $container;
        return $this;
    }
    public function setMaxDepth($maxDepth){
        $this->_maxDepth = $maxDepth;
        return $this;
    }
    public function setTitledMenu($titledMenu = true){
        $this->_titledMenu = $titledMenu;
        return $this;
    }
    public function setTitleClass($titleClass){
        $this->_titleClass = $titleClass;
        return $this;
    }
    public function setTitleTag($titleTag){
        $this->_titleTag = $titleTag;
        return $this;
    }

    public function renderSubMenu($container, $maxDepth, $nestedUlClass){
        $nav2 = new CMS_View_Helper_Nav();
        $nav2->nav($container)->setMaxDepth($maxDepth)->setUlClass($nestedUlClass);
        return $nav2->renderMenu();
    }
    protected function canBeRendered($page){
        if($page->visible){
            if($this->_useAcl){
                if($this->_acl) {
                    if ($this->_acl->isAllowed(null, $page->resource)) return true;
                }
                else throw new CMS_View_Exception('Acl is not set');
            }
            else return true;
        }
        return false;
    }

    public function breadcrumbs(){
        $b = new CMS_View_Helper_Nav_Breadcrumbs();
        return $b->breadcrumbs();
    }

    public function renderMenu(){
        $res = '';
        if(!$this->_container){
            throw new CMS_View_Exception('Navigation container is not set');
        }
        $this->_container->findDeepestActivePage();
        if($this->_container->hasPages()){
            $res .= '<ul class="'.$this->_ulClass.'">';
            foreach($this->_container as $page){
                if($this->canBeRendered($page)) {
                    $active = ($page->isActive()) ? $this->_activeClass : '';
                    $res .= '<li class="'.$active.'"><a href="' . $page->getHref() . '">' . $page->title . '</a></li>';
                    if ($this->_maxDepth != 0) {
                        if ($page->hasPages()) {
                            $res .= $this->renderSubMenu($page, $this->_maxDepth - 1, $this->_nestedUlClass);
                        }
                    }
                }
            }
            $res .= '</ul>';
            if($res == '<ul class="'.$this->_ulClass.'"></ul>') $res = '';
        }
        return $res;
    }

    public function renderTitledMenu(){
        $res = '';
        if(!$this->_container){
            throw new CMS_View_Exception('Navigation container is not set');
        }
        $this->_container->findDeepestActivePage();
        if($this->_container->hasPages()){
            $res .= '';
            foreach($this->_container as $page){
                if($this->canBeRendered($page)) {
                    $active = ($page->isActive()) ? $this->_activeClass : '';
                    $res .= '<'.$this->_titleTag.' class="'.$this->_titleClass.'">' . $page->title . '</'.$this->_titleTag.'>';
                    if ($page->hasPages()) {
                        $res .= $this->renderSubMenu($page, 0, $this->_ulClass);
                    }
                }
            }
            if($res == '<ul class="'.$this->_ulClass.'"></ul>') $res = '';
        }
        return $res;
    }

    public function __toString(){
        if(!$this->_titledMenu)
            return $this->renderMenu();
        else
            return $this->renderTitledMenu();
    }

}