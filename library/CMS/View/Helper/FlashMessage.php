<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 14:33
 */

class CMS_View_Helper_FlashMessage extends CMS_View_Helper_Message
{
    /**
     * @var array
     */
    protected $messages = array();

    /**
     * @param string|array $message
     * @return CMS_View_Helper_Message
     */
    public function flashMessage($message = null)
    {
        $messages = $this->getMessenger()->getMessages();
        $this->addMessage($messages);
        return $this;
    }

    /**
     * @param string|array $message
     * @return CMS_View_Helper_FlashMessage
     */
    public function addMessage($message)
    {
        if (!is_array($message)) {
            $message = array($message);
        }
        foreach ($message as $msg) {
            $this->messages[] = $msg;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function render()
    {
        $render = array();
        foreach ($this->messages as $message) {
            $render[] = parent::message($message);
        }
        $str = implode(PHP_EOL, $render);
        return $str;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
    protected function getMessenger()
    {
        return Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
    }
}