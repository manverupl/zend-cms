<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 14:26
 */

abstract class CMS_View_Helper_Abstract extends Zend_View_Helper_Abstract
{
    /**
     * @return CMS_View
     */
    public function getView(){
        return $this->view;
    }

    /**
     * @return null|Zend_Layout
     * @throws CMS_View_Exception
     */
    protected function getLayout()
    {
        $layout = Zend_Layout::getMvcInstance();
        if (!$layout) {
            throw new CMS_View_Exception('Layout is not present (getMvcInstance() returned NULL).');
        }
        return $layout;
    }

    /**
     * @return Zend_Controller_Front
     */
    public function getFrontController()
    {
        return Zend_Controller_Front::getInstance();
    }

    /**
     * @return Zend_Config
     */
    public function getApplicationOptions()
    {
        return new Zend_Config($this->getFrontController()->getParam('bootstrap')->getOptions());
    }

    /**
     * @return string
     */
    public function render()
    {
    }

    /**
     * Magic render
     */
    public function __toString()
    {
        try{
            return $this->render();
        } catch(Exception $e){
            trigger_error($e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            return '';
        }
    }
}