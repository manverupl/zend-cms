<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-25 22:34
 */

class CMS_Application_Bootstrap_Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * @return Zend_Controller_Front
     */
    public function getFrontController(){
        return Zend_Controller_Front::getInstance();
    }

    /**
     * @return array|Zend_Config
     */
    public function getOptions()
    {
        return new Zend_Config(parent::getOptions());
    }
}