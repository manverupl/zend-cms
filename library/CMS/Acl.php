<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: HCSerwis v2.0
 * Dnia: 2015-03-25 19:55
 */

class CMS_Acl extends Zend_Acl
{
    protected $defaultRole;

    public function setDefaultRole($role){
        if(!$role instanceof Zend_Acl_Role_Interface && !is_string($role)) throw new Zend_Exception('Invalid role given');
        if(is_string($role)) $role = new Zend_Acl_Role($role);
        $this->defaultRole = $role;
        return $this;
    }
    public function getDefaultRole(){
        return $this->defaultRole;
    }
    public function setCurrentRole($role){
        return $this->setDefaultRole($role);
    }
    public function getCurrentRole()
    {
        return $this->getDefaultRole();
    }
    public function hasDefaultRole()
    {
        return (bool)$this->defaultRole;
    }
    public function getRules($role, $resource)
    {
        if ($role) {
            $role = $this->getRole($role);
        }

        return parent::_getRules(new Zend_Acl_Resource($resource), $role);
    }
    public function isAllowed($role = null, $resource = null, $privilege = null)
    {
        if ($role === null) {
            $role = $this->getDefaultRole();
        }

        return parent::isAllowed($role, $resource, $privilege);
    }
    public function getAllRules($resource)
    {
        $prvlgs = array();
        foreach ($this->getRoles() as $role) {
            $tmp = $this->getRules($role, $resource);

            if (isset($tmp['allPrivileges'])) {
                $prvlgs['*'] = array(
                    'name' => '*',
                    'assert' => (isset($tmp['allPrivileges']['assert']) ? $tmp['allPrivileges']['assert'] : null)
                );
            }

            if (isset($tmp['byPrivilegeId']) && !empty($tmp)) {
                foreach ($tmp['byPrivilegeId'] as $actionName => $action) {
                    $prvlgs[$actionName] = array('name' => $actionName, 'assert' => $action['assert']);
                }
            }
        }

        return $prvlgs;
    }
    public function getPlugin()
    {
        return new CMS_Acl_Plugin($this);
    }
}