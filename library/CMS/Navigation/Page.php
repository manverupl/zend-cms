<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-04-01 17:56
 */

class CMS_Navigation_Page extends CMS_Navigation_Container
{
    /**
     * @var array
     */
    protected $_options = array(
        'title' => 'Untitled page',
        'label' => '',
        'sublabel' => '',
        'route' => '',
        'visible' => true,
        'mvc' => null
    );
    /**
     * @var CMS_Navigation_Container|CMS_Navigation_Page
     */
    protected $_parent;
    /**
     * @var string
     */
    protected $_href;
    /**
     * @var bool
     */
    protected $_active = false;

    /**
     * @param array|Zend_Config $params
     * @throws CMS_Navigation_Exception
     */
    public function __construct($params){
        if($params instanceof Zend_Config){
            $params = $params->toArray();
        }
        elseif(!is_array($params)){
            throw new CMS_Navigation_Exception('Invalid argument. $params must be an array or instance of Zend_Config');
        }

//        Zend_Debug::dump($params);
        foreach($params as $key => $value){
            if($key == 'pages') $this->addPages($value);
            else{
                $this->_options[$key] = $value;
            }
        }
    }

    public function setParent($container){
        if(!$container instanceof CMS_Navigation_Page && !$container instanceof CMS_Navigation_Container){
//            Zend_Debug::dump($container);
            throw new CMS_Navigation_Exception('Invalid argument. $container must be an instance of CMS_Navigation_Page or CMS_Navigation_Container.');
        }
        if($container === $this){
            throw new CMS_Navigation_Exception('Page cannot be a parent to itself.');
        }
        $this->_parent = $container;
        return $this;
    }

    public function getParent(){
        return $this->_parent;
    }

    public function hashCode(){
        return spl_object_hash($this);
    }

    public function setTitle($title){
        $this->_options['title'] = $title;
        return $this;
    }
    public function getTitle(){
        return $this->_options['title'];
    }

    public function getHref(){
        if($this->_href == '') $this->setHref();
        return $this->_href;
    }

    public function __get($name){
        if(isset($this->_options[$name])) return $this->_options[$name];
        return null;
    }
    public function __set($name, $value){
        $this->_options[$name] = $value;
    }

    public function hasParam($name){
        if(isset($this->_options[$name])){
            if($this->_options[$name] != null) {
                if ($this->_options[$name] != '') return true;
            }
        }
        return false;
    }

    public function isActive(){
        return $this->_active;
    }
    protected function setActive($active = true){
        $this->_active = $active;
    }

    public function setHref(){
        $urlHelper = new CMS_View_Helper_Route();
        if(!$this->hasParam('route')){
            if(!$this->hasParam('mvc')){
                throw new CMS_Navigation_Exception('Cant generate href. Route or MVC parameters needed.');
            }
            if(!is_array($this->_options['mvc'])){
                throw new CMS_Navigation_Exception('MVC parameters should be an array');
            }
            $mvc = $this->_options['mvc'];
            if(!array_key_exists('action', $mvc)){
                throw new CMS_Navigation_Exception('MVC should contain key action in it');
            }
            $urlHelper = new Zend_View_Helper_Url();
            $this->_href = $urlHelper->url($mvc, null);
        }
        else
        {
            $this->_href = $urlHelper->route($this->_options['route']);
        }

    }
    public function checkIsActive(){
        if(!$this->hasParam('route')){
            if(!$this->hasParam('mvc')){
                throw new CMS_Navigation_Exception('Cant check activity. Route or MVC parameters needed.');
            }
            if(!is_array($this->_options['mvc'])){
                throw new CMS_Navigation_Exception('MVC parameters should be an array');
            }
            $mvc = $this->mvc;
            $r = Zend_Controller_Front::getInstance()->getRouter();
            $route = $r->getCurrentRoute()->getDefaults();
            if($mvc == $route) $this->_active = true;
        }
        else{
            $r = Zend_Controller_Front::getInstance()->getRouter();
            $route = $r->getCurrentRouteName();
            if($this->route == $route){
                $this->_active = true;
                $par = $this->_parent;
                while($par){
                    if($par instanceof CMS_Navigation_Page) {
                        $par->setActive();
                        $par = $par->_parent;
                    }
                    else $par = null;
                }
            }
        }
    }
}