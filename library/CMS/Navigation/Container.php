<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-04-01 17:55
 */

class CMS_Navigation_Container implements Countable, Iterator
{
    /**
     * @var CMS_Navigation_Page[]
     */
    protected $_pages = array();

    public function addPage($page){
        if($this === $page){
            throw new CMS_Navigation_Exception('Page cannot contain itself!');
        }
        if(is_array($page) || $page instanceof Zend_Config){
            $page = new CMS_Navigation_Page($page);
        }
        elseif(!$page instanceof CMS_Navigation_Page){
            throw new CMS_Navigation_Exception('Invalid argument. $page must be an instance of CMS_Navigation_Page or Zend_Config, or an array.');
        }
        $hash = $page->hashCode();
        if(array_key_exists($hash, $this->_pages)){
            return $this;
        }

        $this->_pages[$hash] = $page;
        $page->setParent($this);
        return $this;
    }

    public function addPages($pages){
        if ($pages instanceof Zend_Config) {
            $pages = $pages->toArray();
        }
        if(!is_array($pages)){
            throw new CMS_Navigation_Exception('Invalid argument. $pages must be an array or instance of Zend_Config.');
        }
//        Zend_Debug::dump($pages);
        foreach($pages as $page){
            $this->addPage($page);
        }
        return $this;
    }

    public function clearPages(){
        $this->_pages = array();
        return $this;
    }

    public function removePage($page, $recursive = true){
        if(!$page instanceof CMS_Navigation_Page){
            throw new CMS_Navigation_Exception('Invalid argument. $page must be an instance of CMS_Navigation_Page.');
        }
        if($page === $this){
            throw new CMS_Navigation_Exception('The page cannot remove itself.');
        }
        $hash = $page->hashCode();
        if(!array_key_exists($hash, $this->_pages)){
            if($recursive) {
                foreach ($this->_pages as $p) {
                    if ($p->removePage($page)) return true;
                }
            }
        }
        else{
            unset($this->_pages[$hash]);
            return true;
        }
        return false;
    }

    public function hasPages(){
        return (count($this->_pages) > 0);
    }

    public function hasPage($page, $recursive = true){
        if(!$page instanceof CMS_Navigation_Page){
            throw new CMS_Navigation_Exception('Invalid argument. $page must be an instance of CMS_Navigation_Page.');
        }
        if($page === $this){
            throw new CMS_Navigation_Exception('Page cannot contain itself.');
        }
        $hash = $page->hashCode();
        if(!array_key_exists($hash, $this->_pages)){
            if($recursive) {
                foreach ($this->_pages as $p) {
                    if ($p->hasPage($page)) return true;
                }
            }
        }
        else{
            return true;
        }
        return false;
    }

    protected function findOneByParam($param, $value){
        $result = null;
        if($this instanceof CMS_Navigation_Page){
            if($this->hasParam($param)){
                if($this->$param == $value) $result = $this;
            }
            else{
                if($this->hasPages()) {
                    foreach ($this->_pages as $page) {
                        if($page->findByParam($param, $value)) {
                            $result = $page->findOneByParam($param, $value);
                        }
                    }
                }
            }
        }
        else{
            foreach($this->_pages as $page){
                if($page->findByParam($param, $value))
                    $result = $page->findOneByParam($param, $value);
            }
        }
        return $result;
    }

    public function findByParam($param, $value){
       return $this->findOneByParam($param, $value);
    }

    public function findDeepestActivePage(){
        if($this instanceof CMS_Navigation_Page){
            if($this->isActive() && !$this->hasPages()) return $this;
            else{
                foreach($this->_pages as $page){
                   if($res = $page->findDeepestActivePage()) return $res;
                }
            }
        }
        else{
            $this->makeActivePages();
            foreach($this->_pages as $page){
                if($res = $page->findDeepestActivePage()) return $res;
            }
        }
        return null;
    }
    public function makeActivePages(){
        if($this->hasPages()){
            foreach($this->_pages as $page){
                $page->makeActivePages();
            }
        }
        if($this instanceof CMS_Navigation_Page)
        {
            $this->checkIsActive();
        }
    }

    public function count(){
        return count($this->_pages);
    }

    public function current()
    {
        return current($this->_pages);
    }

    public function next()
    {
        return next($this->_pages);
    }

    public function key()
    {
        return key($this->_pages);
    }

    public function valid()
    {
        $key = key($this->_pages);
        return ($key !== NULL && $key !== FALSE);
    }

    public function rewind()
    {
        reset($this->_pages);
    }

}