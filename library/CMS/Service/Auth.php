<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: HCSerwis v2.0
 * Dnia: 2015-03-25 13:03
 */

class CMS_Service_Auth extends CMS_Service_Auth_Auth
{
    public $tableName = 'user';
    public $identityColumn = 'email';
    public $credentialColumn = 'password';
    public $emailIdentityColumn = 'email';

    /**
     * @return Zend_Auth_Adapter_DbTable
     * @throws Zend_Exception
     */
    public function getAdapter()
    {
        if(!$this->adapter){
            $adapter = new Zend_Auth_Adapter_DbTable(
                $this->getServiceManager()->offsetGet('db'),
                $this->tableName,
                $this->identityColumn,
                $this->credentialColumn
            );
            parent::setAdapter($adapter);
        }else{
            if(!($this->adapter instanceof Zend_Auth_Adapter_DbTable)){
                throw new Zend_Exception('Adapter should be an instance of
					Zend_Auth_Adapter_DbTable but "'.get_class($this->adapter).'" was given');
            }
        }
        return parent::getAdapter();
    }
    public function authenticate($username, $password)
    {
        $username = (string) $username;
        $password = $this->saltPassword((string) $password);

        $adapter = $this->getAdapter()
            ->setIdentity($username)
            ->setCredential($password);

        if(preg_match('*.@.*', $username)){
            $adapter->setIdentityColumn('email');
        }

        $authResult = $adapter->authenticate();
        $data = $adapter->getResultRowObject(null, $this->credentialColumn);

        if($authResult->isValid() === true){
            $this->regenerateId();
            $this->getStorage()->write($data);
            return true;
        }

        $this->result = $authResult;
        return false;
    }
}