<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: HCSerwis v2.0
 * Dnia: 2015-03-25 12:47
 */

abstract class CMS_Service_Auth_Auth extends CMS_Service_Abstract
{
    protected $salt;
    protected $storage;
    protected $useSalt = true;
    protected $hashAlgorithm = 'SHA1';
    protected $adapter;
    protected $result;

    public function __construct(Zend_Auth_Adapter_Interface $adapter = null)
    {
        if ($adapter) {
            $this->setAdapter($adapter);
        }
    }
    public function setAdapter(Zend_Auth_Adapter_Interface $adapter)
    {
        $this->adapter = $adapter;
        return $this;
    }
    public function getAdapter()
    {
        return $this->adapter;
    }
    public function setHashAlgorithm($hashAlgorithm)
    {
        $this->hashAlgorithm = $hashAlgorithm;

        return $this;
    }
    public function getHashAlgorithm()
    {
        return $this->hashAlgorithm;
    }
    public function getSalt()
    {
        return $this->salt;
    }
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return Zend_Auth_Result
     */
    public function getResult()
    {
        return $this->result;
    }
    public function getErrorCode()
    {
        return $this->getResult()->getCode();
    }
    /**
     * Set storage
     *
     * @param \Zend_Auth_Storage_Interface $storage
     * @return \CMS_Service_Auth_Auth
     */
    public function setStorage(Zend_Auth_Storage_Interface $storage)
    {
        Zend_Auth::getInstance()->setStorage($storage);
        $this->storage = $storage;
        return $this;
    }

    /**
     * Get storage
     *
     * @return Zend_Auth_Storage_Interface
     */
    public function getStorage()
    {
        if($this->storage === null){
            $storage = new Zend_Auth_Storage_Session('Zend_Auth', 'storage');
            $this->setStorage($storage);
        }
        return $this->storage;
    }

    /**
     * @param boolean $useSalt
     * @return \CMS_Service_Auth_Auth
     */
    public function setUseSalt($useSalt)
    {
        $this->useSalt = $useSalt;

        return $this;
    }
    public function getUseSalt()
    {
        return $this->useSalt;
    }
    public function saltPassword($value)
    {
        if (!$this->getUseSalt()) {
            return $value;
        }
        $value = $value . $this->getSalt();
        $method = strtolower($this->getHashAlgorithm());
        switch ($method) {
            case 'md5':
                $algo = 'MD5';
                break;
            case 'sha-256':
            case 'sha256':
                $algo = 'SHA-256';
                break;
            case 'sha1':
                $algo = 'sha1';
                break;
            default:
                $algo = 'MD5';
                break;
        }

        return hash($algo, $value);
    }
    public abstract function authenticate($username, $password);

    public function regenerateId()
    {
        if (!Zend_Session::isRegenerated()) {
            Zend_Session::regenerateId();
        }

        return $this;
    }
    public function hasIdentity()
    {
        return Zend_Auth::getInstance()->hasIdentity();
    }

    public function getIdentity()
    {
        return Zend_Auth::getInstance()->getIdentity();
    }
    public function clearIdentity()
    {
        Zend_Auth::getInstance()->clearIdentity();
        return $this;
    }
}