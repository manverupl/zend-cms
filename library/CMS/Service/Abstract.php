<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: HCSerwis v2.0
 * Dnia: 2015-03-25 12:48
 */

abstract class CMS_Service_Abstract
{
    protected function getServiceManager(){
        return Zend_Registry::getInstance();
    }
    public function getFrontController(){
        return Zend_Controller_Front::getInstance();
    }
    public function getView(){
        if($this->getLayout()){
            return $this->getLayout()->getView();
        }
        return null;
    }
    public function getLayout(){
        return Zend_Layout::getMvcInstance();
    }
    protected function getBootstrap()
    {
        if ($this->_bootstrap === null) {
            $this->_bootstrap = $this->getFrontController()->getParam('bootstrap');
        }
        return $this->_bootstrap;
    }
    protected function getRequest()
    {
        return $this->getFrontController()->getRequest();
    }
    protected function getResponse()
    {
        return $this->getFrontController()->getResponse();
    }
    public function getApplicationOptions()
    {
        return $this->getBootstrap()->getOptions();
    }
}