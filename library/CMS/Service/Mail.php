<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-31 17:33
 *
 */

class CMS_Service_Mail extends Zend_Mail
{
    /**
     * @var Zend_View
     */
    protected $view;
    protected $templateName;
    protected $charset = 'utf-8';

    public function __construct(Zend_View_Interface $view){
        $this->view = $view;
        parent::__construct($this->charset);
    }

    /**
     * @return Zend_View
     */
    public function getView(){
        return $this->view;
    }

    /**
     * @param string $name
     * @return CMS_Service_Mail
     */
    public function setViewScript($name){
        $this->templateName = $name;
        return $this;
    }

    /**
     * @param array $properties
     * @throws Zend_View_Exception
     * @return CMS_Service_Mail
     */
    public function assign(array $properties){
        $this->getView()->assign($properties);
        return $this;
    }

    /**
     * @param string $recipient
     * @param $subject
     * @param $template
     * @return CMS_Service_Mail
     * @throws Zend_Mail_Exception
     */
    public function send($recipient = null, $subject = null, $template = null){
        if($recipient) $this->addTo($recipient);
        if($subject) $this->setSubject($subject);
        if($template) $this->templateName = $template;
        if($this->templateName) $this->setBodyHtml($this->view->render($this->templateName), $this->charset);

        return parent::send();
    }
}