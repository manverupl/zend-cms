<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 12:26
 */

class CMS_Service_Navigation
{
    protected $container;
    protected $viewHelper;

    public function __construct(Zend_Navigation $navigation = null)
    {
        if ($navigation) {
            $this->setContainer($navigation);
        }
    }
    public function setAcl(Zend_Acl $acl, $defaultRole = null)
    {
        if ($this->hasViewHelper()) {
            $this->getViewHelper()->setUseAcl(1)->setAcl($acl)->setDefaultRole($defaultRole);
        }

        return $this;
    }
    public function setDefaultAclRole($role)
    {
        if ($this->hasViewHelper()) {
            $this->getViewHelper()->setUseAcl(1)->setDefaultRole($role);
        }

        return $this;
    }
    public function setAclRole($role)
    {
        if ($this->hasViewHelper() && $this->getViewHelper()->getUseAcl() == true) {
            $this->getViewHelper()->setRole($role);
        }

        return $this;
    }
    public function getContainer()
    {
        if (!$this->container) {
            throw new CMS_Service_Navigation_Exception('Missing navigation container');
        }

        return $this->container;
    }
    public function setContainer(Zend_Navigation_Container $container = null)
    {
        $this->container = $container;
        if ($this->hasViewHelper()) {
            $this->getViewHelper()->setContainer($this->container);
        }

        return $this;
    }
    public function setViewHelper(Zend_View_Helper_Navigation $helper)
    {
        $this->viewHelper = $helper;
        if ($this->container) {
            $this->viewHelper->setContainer($this->container);
        }

        return $this;
    }

    /**
     * @return Zend_View_Helper_Navigation
     * @throws CMS_Service_Navigation_Exception
     */
    public function getViewHelper()
    {
        if (!$this->viewHelper) {
            throw new CMS_Service_Navigation_Exception('No view helper found !');
        }

        return $this->viewHelper;
    }
    public function hasViewHelper()
    {
        return !empty($this->viewHelper);
    }
    public function findDeepestActivePage()
    {
        if (!$this->hasViewHelper()) {
            return null;
        }
        $renderInvisible = $this->getViewHelper()->getRenderInvisible();
        $result = $this->getViewHelper()->setRenderInvisible(1)->findActive($this->getContainer());
        $this->getViewHelper()->setRenderInvisible($renderInvisible);
        if (empty($result)) {
            return null;
        }

        return $result['page'];
    }
}