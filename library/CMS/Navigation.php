<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-04-02 17:22
 */

class CMS_Navigation extends CMS_Navigation_Container
{
    /**
     * @param array|Zend_Config|null $pages
     * @throws CMS_Navigation_Exception
     */
    public function __construct($pages = null){
        if(!is_array($pages) && !$pages instanceof Zend_Config){
            throw new CMS_Navigation_Exception("Invalid argument. $pages must be an array or an instance of Zend_Config.");
        }
        elseif($pages != null) $this->addPages($pages);
    }
}