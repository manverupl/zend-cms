<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 00:11
 *
 * @method string route($routeName = null, array $options = null, $reset = false, $encode = true)
 * @method Zend_View_Helper_Navigation navigation()
 * @method string baseUrl($relativePath = null)
 * @method CMS_View_Helper_Page page()
 * @method string message($msg)
 * @method string flashMessage()
 * @method CMS_View_Helper_Dupa dupa($container = null)
 * @method CMS_View_Helper_Nav nav()
 *
 */

class CMS_View extends Zend_View
{
    /**
     * @var Service_ModelRepository
     */
    protected $_model;
    /**
     * @var CMS_Acl
     */
    protected $acl;
    /**
     * @var CMS_Navigation
     */
    protected $_navigation;

    /**
     * @return Service_ModelRepository
     */
    public function getModel(){
        return $this->_model;
    }

    public function setModel($model){
        $this->_model = $model;
        return $this;
    }
    public function setAcl(CMS_Acl $acl)
    {
        $this->acl = $acl;
        return $this;
    }
    public function getAcl()
    {
        if (!$this->acl) {
            throw new CMS_View_Exception('ACL is not available');
        }
        return $this->acl;
    }

    public function getNavigation()
    {
        if (!$this->_navigation) {
            throw new CMS_View_Exception('Navigation is not available');
        }
        return $this->_navigation;
    }

    public function setNavigation(CMS_Navigation $navigation)
    {
        $this->_navigation = $navigation;
        return $this;
    }


}