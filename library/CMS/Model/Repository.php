<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 00:24
 */

abstract class CMS_Model_Repository
{
    protected $adapter;

    public function __construct(Zend_Db_Adapter_Abstract $adapter = null)
    {
        if($adapter !== null){
            CMS_Model_DbTable_Abstract::setDefaultAdapter($adapter);
            $this->setAdapter($adapter);
        }
    }

    public function setAdapter($adapter)
    {
        $this->adapter = $adapter;
        return $this;
    }

    public function getAdapter()
    {
        return $this->adapter;
    }
}