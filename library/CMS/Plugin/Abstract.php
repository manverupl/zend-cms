<?php

/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 14:33
 */

abstract class CMS_Plugin_Abstract extends Zend_Controller_Plugin_Abstract
{
    /**
     * @return Zend_Controller_Front
     */
    public function getFrontController()
    {
        return Zend_Controller_Front::getInstance();
    }

    /**
     * @return Bootstrap
     */
    public function getBootstrap()
    {
        $this->getFrontController()->getParam('bootstrap');
    }
}