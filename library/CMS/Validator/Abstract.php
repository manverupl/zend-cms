<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-28 10:54
 */

abstract class CMS_Validator_Abstract extends Zend_Validate_Abstract
{
    protected $_options = array();

    public function __construct($options = null)
    {
        if ($options !== null) {
            $this->setOptions($options);
        }
    }

    /**
     * Add error message template
     *
     * @param string $const
     * @param string $message
     * @return CMS_Validator_Abstract
     */
    protected function addMessage($const, $message)
    {
        $this->_messageTemplates[$const] = $message;

        return $this;
    }

    /**
     * Add message templates
     *
     * @param array $messages
     * @return CMS_Validator_Abstract
     */
    protected function addMessages(array $messages)
    {
        foreach ($messages as $id => $message) {
            $this->addMessage($id, $message);
        }

        return $this;
    }

    /**
     * Get option
     *
     * @param string $name
     * @return mix|null if option was not found
     */
    public function getOption($name)
    {
        if (isset($this->_options[$name])) {
            return $this->_options[$name];
        }

        return null;
    }

    /**
     * Get validator options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * Set option
     *
     * @param string $name
     * @param mix    $value
     * @return CMS_Validator_Username
     */
    public function setOption($name, $value)
    {
        $this->_options[$name] = $value;

        return $this;
    }


    /**
     * Set validator options
     *
     * @param $options
     * @return CMS_Validator_Abstract
     */
    public function setOptions($options = null)
    {
        if (!is_null($options)) {
            if ($options instanceof Zend_Config) {
                $options = $options->toArray();
            }
            $this->_options = array_merge($this->_options, $options);
        }

        return $this;
    }
}