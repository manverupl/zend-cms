<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-28 10:56
 */

class CMS_Validator_Password extends CMS_Validator_Abstract
{
    const ERR_PASS_TOO_SHORT = 'errPassTooShort';
    const ERR_PASS_NO_MATCH = 'errPassNoMatch';
    const ERR_PASS_TOO_LONG = 'errPassToLong';
    const ERR_PASS_NO_NUM = 'errPassNoNum';
    const ERR_PASS_NO_SPEC = 'errPassNoSpec';
    const ERR_PASS_NO_CAP = 'errPassNoCap';
    const ERR_PASS_NO_SMALL = 'errPassNoSmall';

    protected $_options = array(
        'contextCompare' => false,
        'inputName' => 'password_repeat',
        'checkLength' => true,
        'minLength' => 6,
        'maxLength' => 0,
        'requiredNumber' => true,
        'requiredSpecialChar' => false,
        'requiredCapitalLetter' => true,
    );

    protected $_messageVariables = array(
        'min' => 'min'
    );

    protected $min = 4;

    /**
     * Create new password validator instance.
     * Avilable options:
     * bool        contextCompare            compare password and passwordRepeatInputName
     * string    inputName                Input to find within context given
     * string    checkLength                check password lenght
     * string    minLength                if checkLength used set min lenght for password
     *
     * @param array $options
     */
    public function __construct($options = null)
    {
        $this->setOptions($options);
        $this->addMessage(self::ERR_PASS_NO_MATCH, 'Passwords don\'t match.');
        $this->addMessage(self::ERR_PASS_TOO_SHORT, 'Password is to short.');
        $this->addMessage(self::ERR_PASS_TOO_LONG, 'Password is to long.');
        $this->addMessage(self::ERR_PASS_NO_NUM, 'Password needs at least one number in it.');
        $this->addMessage(self::ERR_PASS_NO_SPEC, 'Password needs at least one special character(!@#$%^&*()_+-=) in it.');
        $this->addMessage(self::ERR_PASS_NO_CAP, 'Password needs at least one capital letter in it.');
        $this->addMessage(self::ERR_PASS_NO_SMALL, 'Password needs at least one small letter in it');
    }

    /**
     * Tell whenever given password is valid
     *
     * @param string $value
     * @param array  $context
     * @return bool
     */
    public function isValid($value, $context = null)
    {
        $value = (string)$value;
        // check lenght if option set
        if ((bool)$this->getOption('checkLength')) {
            $minLength = (int)$this->getOption('minLength');
            $this->min = $minLength;
            if (strlen($value) < $minLength) {
                $this->_error(self::ERR_PASS_TOO_SHORT);

                return false;
            }
            $maxLength = (int)$this->getOption('maxLength');
            if($maxLength != 0){
                if(strlen($value) > $maxLength){
                    $this->_error(self::ERR_PASS_TOO_LONG);
                    return false;
                }
            }
        }
        if((bool)$this->getOption('requiredNumber')){
            if(!preg_match('/[0-9]{1}/is', $value, $dig)){
                $this->_error(self::ERR_PASS_NO_NUM);
                return false;
            }
        }
        if((bool)$this->getOption('requiredSpecialChar')){
            if(!preg_match('/[\!\@\#\$\%\^\&\*\(\)\_\+\-\=]{1}is', $value, $dig)){
                $this->_error(self::ERR_PASS_NO_SPEC);
                return false;
            }
        }
        if((bool)$this->getOption('requiredCapitalLetter')){
            if(!preg_match('/[A-Z]{1}/s', $value, $dig)){
                $this->_error(self::ERR_PASS_NO_CAP);
                return false;
            }
        }
        if(!preg_match('/[a-z]{1}/s', $value, $letter)){
            $this->_error(self::ERR_PASS_NO_SMALL);
            return false;
        }
        // compare context
        if ((bool)$this->getOption('contextCompare')) {
            $contextKey = $this->getOption('inputName');
            $contextInputValue =
                (isset($context[$contextKey]) ? ($context[$contextKey]) : null);

            if ($value == $contextInputValue) {
                return true;
            } else {
                $this->_error(self::ERR_PASS_NO_MATCH);

                return false;
            }
        }

        return true;
    }
}