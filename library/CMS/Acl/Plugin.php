<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: HCSerwis v2.0
 * Dnia: 2015-03-25 20:00
 */

class CMS_Acl_Plugin extends Zend_Controller_Plugin_Abstract
{
    protected $acl;

    /**
     * @var CMS_Service_Auth
     */
    protected $auth;

    /**
     * The resource prefix.
     *
     * @var string
     */
    protected $_resourcePrefix = '';

    /**
     * The resource separator.
     *
     * @var string
     */
    protected $_resourceSeparator = '.';

    /**
     * Creates an ACL plugin.
     *
     * @todo change $acl to required param
     * @param \CMS_Acl $acl
     */
    public function __construct(CMS_Acl $acl = null)
    {
        if ($acl) {
            $this->setAcl($acl);
        }
    }

    /**
     * Tell if current module can handle errors
     *
     * @param $module
     * @return bool
     */
    protected function canHandleErrors($module)
    {
        $f = $this->getFrontController();
        $testRequest = new Zend_Controller_Request_Http();
        $testRequest
            ->setActionName($f->getDefaultAction())
            ->setControllerName($this->getErrorHandler()->getErrorHandlerAction())
            ->setModuleName($module);

        return $f->getDispatcher()->isDispatchable($testRequest);
    }

    /**
     * @param Zend_Acl $acl
     * @return CMS_Acl_Plugin
     */
    public function setAcl(Zend_Acl $acl)
    {
        $this->acl = $acl;

        return $this;
    }

    /**
     * @throws Zend_Exception
     * @return CMS_Acl
     */
    public function getAcl()
    {
        if (null === $this->acl) {
            throw new CMS_Acl_Exception('Missing ACL object');
        }

        return $this->acl;
    }

    /**
     * @param \CMS_Service_Auth $auth
     * @return CMS_Acl_Plugin
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;

        return $this;
    }

    /**
     * @return \CMS_Service_Auth
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * @return Zend_Acl_Role_Interface
     */
    public function getRole()
    {
        return $this->getAcl()->getDefaultRole();
    }

    /**
     * @param $prefix
     * @return CMS_Acl_Plugin
     */
    public function setResourcePrefix($prefix)
    {
        $this->_resourcePrefix = (string)$prefix;

        return $this;
    }

    /**
     * @return string
     */
    public function getResourcePrefix()
    {
        return $this->_resourcePrefix;
    }

    /**
     * @param $separator
     * @return CMS_Acl_Plugin
     */
    public function setResourceSeparator($separator)
    {
        $this->_resourceSeparator = (string)$separator;

        return $this;
    }

    /**
     * @return string
     */
    public function getResourceSeparator()
    {
        return $this->_resourceSeparator;
    }

    /**
     * @return Zend_Controller_Plugin_ErrorHandler
     */
    protected function getErrorHandler()
    {
        return $this->getFrontController()->getPlugin('Zend_Controller_Plugin_ErrorHandler');
    }

    /**
     * @return Zend_Controller_Front
     */
    protected function getFrontController()
    {
        return Zend_Controller_Front::getInstance();
    }

    /**
     * @param      $module
     * @param bool $controller
     * @param bool $action
     * @return string
     */
    public function getResourceId($module, $controller = false, $action = false)
    {
        $frontController = Zend_Controller_Front::getInstance();

        if (empty($module)) {
            $module = $frontController->getDefaultModule();
        }

        $resourceId = $this->getResourcePrefix() . ((string)$module);

        if (false !== $controller) {
            if (empty($controller)) {
                $controller = $frontController->getDefaultControllerName();
            }

            $resourceId .= $this->getResourceSeparator() . ((string)$controller);

            if (false !== $action) {
                if (empty($action)) {
                    $action = $frontController->getDefaultAction();
                }
                $resourceId .= $this->getResourceSeparator() . ((string)$action);
            }
        }

        return $resourceId;
    }

    /**
     * @param null $resource
     * @param null $privilege
     * @return bool
     */
    public function isAllowed($resource = null, $privilege = null)
    {
        return $this->getAcl()->isAllowed($this->getRole(), $resource, $privilege);
    }

    /**
     * @param      $module
     * @param      $controller
     * @param      $action
     * @param null $privilege
     * @return bool
     */
    public function isActionAllowed($module, $controller, $action, $privilege = null)
    {
        $acl = $this->getAcl();
        $action = $this->getResourceId($module, $controller, $action);
        $controller = $this->getResourceId($module, $controller);
        $module = $this->getResourceId($module);

        if (!$acl->has($module)) {
            $acl->addResource(new Zend_Acl_Resource($module));
        }

        if (!$acl->has($controller)) {
            $acl->addResource(new Zend_Acl_Resource($controller), $module);
        }

        if (!$acl->has($action)) {
            $acl->addResource(new Zend_Acl_Resource($action), $controller);
        }

        return $this->isAllowed($action, $privilege);
    }

    /**
     * @param Zend_Controller_Request_Abstract $request
     * @param null                             $privilege
     * @return bool
     */
    public function isRequestAllowed(Zend_Controller_Request_Abstract $request, $privilege = null)
    {
        if (!Zend_Controller_Front::getInstance()->getDispatcher()->isDispatchable($request)) {
            return true;
        }

        return $this->isActionAllowed(
            $request->getModuleName(),
            $request->getControllerName(),
            $request->getActionName(),
            $privilege
        );
    }

    /**
     * @todo auto-redirect
     * @param Zend_Controller_Request_Abstract $request
     * @throws Zend_Exception
     * @return void
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if ($this->getErrorHandler()) {
            if (
                $this->getErrorHandler()->getErrorHandlerModule() == $request->getModuleName()
                && $this->getErrorHandler()->getErrorHandlerController() == $request->getControllerName()
            ) {
                return;
            }
        }

        if (!$this->isRequestAllowed($request)) {

            $resourceName = $this->getResourceId(
                $request->getModuleName(),
                $request->getControllerName(),
                $request->getActionName()
            );

            $e = null;

            if ($this->getAuth()) {
                if (!$this->getAuth()->hasIdentity()) {
                    $e = new CMS_Acl_Exception('Identity not set.');
                }
            }

            if (!$e) {
                $e = new CMS_Acl_Exception(sprintf(
                    'Role %s does not have permission to access resource: %s',
                    $this->getRole()->getRoleId(),
                    $resourceName
                ));
            }

            throw $e;
        }
    }
}