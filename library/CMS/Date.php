<?php
/**
 * Utworzono przez: Michał Świątek w PhpStorm
 * Dla: Zend CMS v1.0
 * Dnia: 2015-03-26 00:40
 */

class CMS_Date extends Zend_Date
{
    const DATE_MYSQL_DATE = 'YYYY-MM-dd';
    const DATE_MYSQL_DATETIME = 'YYYY-MM-dd HH:mm:ss';

    public function toMysqlDate(){
        return $this->toString(self::DATE_MYSQL_DATETIME);
    }
    /**
     * Returns the actual date as new date object
     *
     * @param  string|Zend_Locale        $locale  OPTIONAL Locale for parsing input
     * @return CMS_Date
     */
    public static function now($locale = null)
    {
        return new CMS_Date(time(), self::TIMESTAMP, $locale);
    }

}