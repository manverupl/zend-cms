
DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `title` VARCHAR(250) NULL,
  `author` VARCHAR(500) NULL,
  `short_text` LONGTEXT NULL,
  `text` LONGTEXT NULL,
  `date_created` DATETIME NULL,
  `date_modified` DATETIME NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Table containing news.';

DROP TABLE IF EXISTS `site`;
CREATE TABLE IF NOT EXISTS `site` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `slug` VARCHAR(50) NOT NULL UNIQUE,
  `title` VARCHAR(200) NULL,
  `column_one` LONGTEXT NULL,
  `column_two` LONGTEXT NULL,
  `date_created` DATETIME NULL,
  `date_modified` DATETIME NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Table containing sites.';

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL UNIQUE,
  `value` VARCHAR(200) NULL,
  `date_modified` DATETIME NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Table containing settings of application.';

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `email` VARCHAR(50) NOT NULL UNIQUE,
  `password` VARCHAR(250) NULL,
  `role` ENUM('superadmin', 'admin', 'user', 'guest') DEFAULT 'user' COMMENT 'Filed containing user role, roles can be modified.',
  `date_registered` DATETIME NULL,
  `date_last_login` DATETIME NULL,
  `date_last_login_fail` DATETIME NULL,
  `date_modified` DATETIME NULL,
  `enabled` TINYINT(1) DEFAULT 1,
  `active` TINYINT(1) DEFAULT 1
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Table containing basic user data eg. email, password.';

DROP TABLE IF EXISTS `user_data`;
CREATE TABLE IF NOT EXISTS `user_data` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `first_name` VARCHAR(150) NULL,
  `last_name` VARCHAR(150) NULL,
  `phone` VARCHAR(15) NULL,
  FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Table containing all user data eg name, phone. More fields can be added.';